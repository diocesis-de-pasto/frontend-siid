import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WindowsPasswordComponent } from './pages/windows-password/windows-password.component';
const routes: Routes = [
    {
    path: '',
    children: [
      {
        path: '',
        component: WindowsPasswordComponent
      },
    ],
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PasswordRoutingModule { }
