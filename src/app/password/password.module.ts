import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PasswordRoutingModule } from './password-routing.module';
import { WindowsPasswordComponent } from './pages/windows-password/windows-password.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    WindowsPasswordComponent
  ],
  imports: [
    CommonModule, PasswordRoutingModule, MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PasswordModule { }
