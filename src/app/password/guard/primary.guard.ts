import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { InjectTokenService } from '../../sidebar/services/inject-token.service';
@Injectable({
  providedIn: 'root'
})
export class PrimaryGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private injectTokenService: InjectTokenService,
    private router: Router) { }
  canActivate(): Observable<boolean> | boolean {
    return this.estado();
  }
  canLoad(): Observable<boolean> | boolean {
    return this.estado();
  }
  canActivateChild(): Observable<boolean> | boolean {
    return this.estado();
  }
  estado() {
    return this.injectTokenService.firsteTimeUser().pipe(
      tap(
        resp => {
          if (resp === true) {
            return true;
          }
          else {
            this.router.navigateByUrl('/session/password')
            return false;
          }
        }
      )
    ) 
    // .subscribe(
    //   resp=>{
    //        if(resp===true){
    //       return true;
    //     }
    //     else {  
    //       this.router.navigateByUrl('/login/lock')
    //       return false
    //     }
    //   }
    // )
    // pipe(
    //   tap(valid=>{
    // if(!valid){
    //   this.router.navigateByUrl('/login')
    //   return false;
    // }
    // if(valid && (localStorage.getItem('ito')==='V')){
    //   this.router.navigateByUrl('/login/lock')
    //   return false;
    // }
    // else {  
    //   return true
    // }
    // if(this.injectTokenService.firsteTimeUser()===false){
    //   this.router.navigateByUrl('/session/password')
    //   return false
    // }else{
    //   return true
    // }
  }
}
