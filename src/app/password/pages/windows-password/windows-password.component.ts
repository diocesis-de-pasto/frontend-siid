import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { objectUser } from '../../../sidebar/services/inject-token.service';
import jwt_decode from 'jwt-decode';

import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { UserService } from 'src/app/shared/components-user/components/user-information/user.service';
@Component({
  selector: 'app-windows-password',
  templateUrl: './windows-password.component.html',
  styleUrls: ['./windows-password.component.css']
})
export class WindowsPasswordComponent implements OnInit {
  fieldTextType: boolean;
  repeatFieldTextType: boolean;
  fieldTextType2: boolean;
  repeatFieldTextType2: boolean;
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleRepeatFieldTextType() {
    this.repeatFieldTextType = !this.repeatFieldTextType;
  }
  toggleFieldTextType2() {
    this.fieldTextType2 = !this.fieldTextType2;
  }
  toggleRepeatFieldTextType2() {
    this.repeatFieldTextType2 = !this.repeatFieldTextType2;
  }
  myForm: FormGroup = new FormGroup({});
  text: string;
  constructor(private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private errorHandlingService: ErrorHandlingService,
  ) {
  }
  validatePasswords() {
    if (this.myForm.value.password === this.myForm.value.passwordtwo) {
      if (!this.myForm.value.password || !this.myForm.value.passwordtwo) {
        this.text = "Las contraseñas deben coincidir"
        return true
      } else {
        return false
      }
    }
    else {
      this.text = "Las contraseñas deben coincidir"
      return true;
    }
  }
  ngOnInit() {
    this.myForm = this.fb.group({
      password: [, [Validators.required]],
      passwordtwo: [, [Validators.required]],
    });
  }
  async recoveryPassword() {
    var petition = await this.errorHandlingService.question('Restablecer contraseña?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'SI', 'NO', 'question');
  }
  async actualizar() {
    const { password } = this.myForm.value;
    let objectUser: objectUser = jwt_decode(localStorage.getItem('x-tq'));
    this.userService.updateState(objectUser.email, password, 1)
      .subscribe(resp => {
        if (resp === true) {
          this.errorHandlingService.positiveAnswer('Contraseña actualizada')
          this.reset()
          this.router.navigateByUrl('/session/dashboard')
        } else {
          this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
        }
      });
  }
  reset() {
    this.myForm.reset();
    this.myForm.markAsPristine;
    this.myForm.markAsTouched;
  }
}
