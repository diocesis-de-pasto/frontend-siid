import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile.routing';

import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProfileComponent } from './pages/profile/profile.component';
import { ComponentsUserModule } from '../shared/components-user/components-user.module';



@NgModule({
  declarations: [

 
     ProfileComponent,
     
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsUserModule
  ]
})
export class UserDataModule { }
