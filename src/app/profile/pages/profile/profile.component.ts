import { Component, ChangeDetectorRef, OnInit, ElementRef, OnDestroy } from '@angular/core';

import {  Router } from '@angular/router';

import { SpinnerComponentService } from '../../../service/spinner-component.service';
import { InjectTokenService } from 'src/app/sidebar/services/inject-token.service';
import jwt_decode from 'jwt-decode';
import { CrudPermiss } from 'src/app/interface/crud-permiss';
@Component({
   selector: 'app-profile',
   templateUrl: './profile.component.html',
   styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

   idUser: string;
   crudDataPerson: CrudPermiss ;
   crudMenisterial: CrudPermiss ;
   crudAcademy: CrudPermiss ;
   crudPastoral: CrudPermiss ;
   crudRecognition: CrudPermiss ;
   crudFamily: CrudPermiss ;
   crudUser: CrudPermiss ;

   profilePermissions:any;
   constructor(private router: Router,private SpinnerComponentService:SpinnerComponentService ,private injectTokenService:InjectTokenService) {
      this.SpinnerComponentService;
   }
   ngOnInit(): void {
      this.permissAdmin();
      this.permiss();
   }
   step = 0;
   ok;z
   setStep(index: number) {
      this.step = index;
   }
   permissAdmin(){
      let objectUsere: any = jwt_decode(localStorage.getItem('x-tq'));
      this.profilePermissions=objectUsere.profilePermissions;   
 
      
      

   }
   permiss(){
      this.crudUser = this.injectTokenService.permission('Usuario');
      this.crudDataPerson = this.injectTokenService.permission('Datos personales');
      this.crudAcademy      = this.injectTokenService.permission('Administración de datos académicos');
      this.crudMenisterial = this.injectTokenService.permission('Administración información ministerial');
      this.crudPastoral = this.injectTokenService.permission('Administración de datos pastorales');
      this.crudRecognition = this.injectTokenService.permission('Administración de reconocimientos');
      this.crudFamily = this.injectTokenService.permission('Administración contactos familiares');
 
      
   
   }
   nextStep() {
      this.step++;
   }
   prevStep() {
      this.step--;
   }
}
