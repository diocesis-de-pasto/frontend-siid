
import { routes } from './module-diocesano-routing.module';
describe('Rutas de modulo-ModuleDiocesanoModule',()=>{    

    
    it('Debe contener la ruta  "principal" en la posición [0]',()=>{
        expect(routes[0]?.path).toEqual('principal')
    });
    it('Debe contener la ruta  "create" en la posición [1]',()=>{
        expect(routes[1]?.path).toEqual('create')
    });
    it('Debe contener la ruta  "edit/:idModule" en la posición [2]',()=>{
        expect(routes[2]?.path).toEqual('edit/:idModule')
    });
    it('Debe contener la ruta  "view/:idRole" en la posición [3]',()=>{
        expect(routes[3]?.path).toEqual('view/:idModule')
    });
})     