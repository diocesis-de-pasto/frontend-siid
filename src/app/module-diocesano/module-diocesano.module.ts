import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleDiocesanoRoutingModule } from './module-diocesano-routing.module';
import { CreateModuleComponent } from './components/create-module/create-module.component';
import { UpdateModuleComponent } from './components/update-module/update-module.component';
import { ListModuleComponent } from './components/list-module/list-module.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewCreateEditComponent } from './pages/view-create-edit/view-create-edit.component';
import { ParentPipe } from './pipe/parent.pipe';
import { CrudPipe } from './pipe/crud.pipe';
@NgModule({
  declarations: [
    ListModuleComponent,
    UpdateModuleComponent,
    CreateModuleComponent,
    ViewCreateEditComponent,
    ParentPipe,
    CrudPipe
  ],
  imports: [
    CommonModule,
    ModuleDiocesanoRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ModuleDiocesanoModule { }
