import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NameService } from 'src/app/service/name.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ModuleService } from '../../service/module.service';
import { Module } from '../../interface/module';
import { InjectTokenService } from '../../../sidebar/services/inject-token.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';
@Component({
  selector: 'app-view-create-edit',
  templateUrl: './view-create-edit.component.html',
  styleUrls: ['./view-create-edit.component.css']
})
export class ViewCreateEditComponent implements OnInit {
  title: string;
  idRole: string;
  view: boolean = false;
  active: number;
  crudModule: CrudPermiss;
  constructor(
    private moduleService: ModuleService,
    private router: Router,
    private activatedRoute: ActivatedRoute

  ) { }
  ngOnInit() {
    
    this.idRole = this.activatedRoute.snapshot.params.idModule;
    if (this.idRole) {
      this.moduleService.getModule(this.idRole).subscribe(
        resp => {
          if (resp === false) {
            this.router.navigate([`/session/dashboard`]);
          } else {
            var view = "/session/module/view";
            var edit = "/session/module/edit";
            this.idRole = this.activatedRoute.snapshot.params.id;
            if ((this.router.url).indexOf(view) === 0) {
              this.view = true;
              this.active = 1;
              this.title = 'Ver módulo';
              
            } else if ((this.router.url).indexOf(edit) === 0) {

              this.active = 2;
              this.view = false;
              this.title = 'Editar módulo';
            }
          }
        }
      )
    } else {
      var create = "/session/module/create";
      if ((this.router.url).indexOf(create) === 0) {


        this.active = 0;
        this.title = 'Añadir módulo';
      }
    }
  }
}
