import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListModuleComponent } from './components/list-module/list-module.component';
import { ViewCreateEditComponent } from './pages/view-create-edit/view-create-edit.component';
export const routes: Routes = [


  {
    path: 'principal',
    component: ListModuleComponent
  },
  {
    path: 'create',
    component: ViewCreateEditComponent
  },
  {
    path: 'edit/:idModule',
    component: ViewCreateEditComponent
  },
  {
    path: 'view/:idModule',
    component: ViewCreateEditComponent
  },
  { path: '**', redirectTo: '/principal' }


];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuleDiocesanoRoutingModule { }
