
import { FormBuilder, ReactiveFormsModule, FormsModule } from '@angular/forms';
import {  ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../../../app.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { CreateModuleComponent } from './create-module.component';

describe('CreateRoleComponent', () => {
    let component: CreateModuleComponent;
    let fixture: ComponentFixture<CreateModuleComponent>;
    beforeEach((() => {
        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule, FormsModule, HttpClientModule, MaterialModule,
            BrowserAnimationsModule, RouterTestingModule],
            declarations: [CreateModuleComponent],
            providers: [FormBuilder]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(CreateModuleComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('Debe de crear un formulario con ocho campos: idModule, nameModule, refModule, urlModule, iconModule, abbreviationModule, description y crud', () => {
        expect(component.myForm.contains('idModule')).toBeTruthy();
        expect(component.myForm.contains('nameModule')).toBeTruthy();
        expect(component.myForm.contains('refModule')).toBeTruthy();
        expect(component.myForm.contains('urlModule')).toBeTruthy();
        expect(component.myForm.contains('iconModule')).toBeTruthy();
        expect(component.myForm.contains('abbreviationModule')).toBeTruthy();
        expect(component.myForm.contains('description')).toBeTruthy();
        expect(component.myForm.contains('crud')).toBeTruthy();

    });

    it('El nombre del rol debe  ser obligatorio', () => {
        const control = component.myForm.get('nameRole');
        control?.setValue('');
        expect(control?.valid).toBeFalsy();
    })

    it('La descripción del módulo debe ser obligatorio', () => {
        const control = component.myForm.get('description');
        control?.setValue('prueba descripción ');
        expect(control?.valid).toBeTruthy();
    })
    it('La abreviación del módulo es obligatoria', () => {
        const control = component.myForm.get('abbreviationModule');
        control?.setValue('prueba descripción ');
        expect(control?.valid).toBeTruthy();
    })
    it('El campo crud debe ser obligario', () => {
        const control = component.myForm.get('crud');
        control?.setValue('prueba descripción ');
        expect(control?.valid).toBeTruthy();
    })
})