import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Person } from 'src/app/profile/interface/data';
import {Router} from "@angular/router"
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { ModuleService } from '../../service/module.service';
@Component({
  selector: 'app-create-module',
  templateUrl: './create-module.component.html',
  styleUrls: ['../../../css/formulario.css'],
  encapsulation: ViewEncapsulation.None,
})
export class CreateModuleComponent implements OnInit {
  public pattern: string='padre' ;
  public personalUser: Person;
  public id: string;
  public reference: any[] = [];
  @Input() permissCrud: any=[];

  public crudList = [
    "C","R","U","D"
  ];
  public myForm: FormGroup = new FormGroup({});
  constructor(private fb: FormBuilder,
    private errorHandlingService: ErrorHandlingService,
    private moduleService:ModuleService,
    private activatedRoute:ActivatedRoute,
    private router: Router
  ) {
  }
  ngOnInit() {
    if(this.activatedRoute.snapshot.params.id){
      this.id = this.activatedRoute.snapshot.params.id;
    };
    this.references();
    this.myForm = this.fb.group({
      idModule:[],
      nameModule:[, [Validators.required]],
      refModule:[],
      urlModule: [],
      iconModule: [],
      abbreviationModule: [, [Validators.required]],
      description:[, [Validators.required]],
      crud:[, [Validators.required]],
    });


    if (this.pattern==='padre') {
      this.myForm.controls.crud.setValidators([]);
      this.myForm.controls.crud.updateValueAndValidity();
    }else{
      this.myForm.controls.crud.setValidators(Validators.required);
      this.myForm.controls.crud.updateValueAndValidity();
    }

  }
 references(){
    this.moduleService.listModulePrincipal().subscribe(
         resp=> {
           this.reference=resp
         }
       )
   }
  verificatePersonal(){
   return true;
  } 
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  reset(){

    this.myForm.reset();

    if (this.pattern==='padre') {
      this.myForm.controls.crud.setValidators([]);
      this.myForm.controls.crud.updateValueAndValidity();
    }else{
      this.myForm.controls.crud.setValidators(Validators.required);
      this.myForm.controls.crud.updateValueAndValidity();
    }

  }
  async createModule() {

    
    if (this.myForm.invalid === true) {
      this.myForm.markAllAsTouched();
      this.errorHandlingService.negativeAnswer('Ingrese los datos requeridos')
    } else {
      if(typeof( this.myForm.value.refModule)==='string'){
        this.reference.forEach(element=>{
        if(element.nameModule=== (  this.myForm.value.refModule)){
          this.myForm.value.refModule=element.idModule;
        }
      });
      }else{
        this.myForm.value.refModule=0;
        this.myForm.value.crud=null;
      }

      var typeModule = this.pattern==='padre'? 'sub':'child';

      this.moduleService.postModule(
        this.myForm.value.nameModule,
        typeModule,
        this.myForm.value.refModule,
        this.myForm.value.urlModule,
        this.myForm.value.iconModule,
        this.myForm.value.abbreviationModule,
        this.myForm.value.description,
        this.myForm.value.crud
      ).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Módulo creado')
            this.reset();
            this.router.navigate(['/session/module/principal'])
          } else {
            this.errorHandlingService.negativeAnswer('El módulo ya esta registrado')
            this.reset();
          }
        }
      );
    }
  }
}
