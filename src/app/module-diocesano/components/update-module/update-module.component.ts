import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Person } from 'src/app/profile/interface/data';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { getUpdatedValue } from 'src/app/shared/components-user/components/functions';
import { ModuleService } from '../../service/module.service';
@Component({
  selector: 'app-update-module',
  templateUrl: './update-module.component.html',
  styleUrls: ['../../../css/formulario.css'],
  encapsulation: ViewEncapsulation.None,
})

export class UpdateModuleComponent implements OnInit {
  personalUser: Person;
  @Input() view: boolean =false;
  idModule: string;
  reference: any[] = [];
  typeModule = [
    "sub", "child"
  ];

  myForm: FormGroup = new FormGroup({});
  constructor(private fb: FormBuilder,
    private errorHandlingService: ErrorHandlingService,
    private moduleService:ModuleService,
    private activatedRoute:ActivatedRoute,
  ) {
  }
  ngOnInit() {
    
    if(this.activatedRoute.snapshot.params.idModule){
      this.idModule = this.activatedRoute.snapshot.params.idModule;
    };
    this.references();
    this.myForm = this.fb.group({
      idModule:[],
      nameModule:[, [Validators.required]],
      typeModule: [, [Validators.required]],
      refModule:[],
      urlModule: [],
      iconModule: [],
      abbreviationModule: [, [Validators.required]],
      description:[, [Validators.required]],
      crud: [],
    });
  }
  getPersonal() {
    this.moduleService.getModule(this.idModule).subscribe(
      resp => {
        
        if (resp!==false) {
          this.reference.forEach(element=>{
            if(element.idModule=== Number(resp.refModule)){
              resp.refModule=element.nameModule;
            }
          });
          this.myForm.controls['idModule'].setValue(resp.idModule);
          this.myForm.controls['nameModule'].setValue(resp.nameModule);
          this.myForm.controls['refModule'].setValue(resp.refModule);
          this.myForm.controls['typeModule'].setValue(resp.typeModule);
          this.myForm.controls['urlModule'].setValue(resp.urlModule);
          this.myForm.controls['iconModule'].setValue(resp.iconModule);
          this.myForm.controls['abbreviationModule'].setValue(resp.abbreviationModule);
          this.myForm.controls['description'].setValue(resp.description);
          this.myForm.controls['crud'].setValue(resp.crud);
        } 
      }
    )
  }
 references(){
    this.moduleService.listModulePrincipal().subscribe(
         resp=> {
           this.reference=resp
           this.getPersonal();
         }
       )
   }
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  async updateModule() {
    var petition = await this.errorHandlingService.question('¿Desea guardar los cambios realizados?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'guardar', 'volver', 'question');
    if (petition === true) {
      var change:any = getUpdatedValue(this.myForm);
      if(change.refModule){
          this.reference.forEach(element=>{
            if(element.nameModule===change.refModule){
              change.refModule=element.idModule;
            }
          })
      }
      this.moduleService.putModule(change,     this.myForm.value.idModule)
        .subscribe(
          resp => {
            if (resp === true) {
              this.errorHandlingService.positiveAnswer('Módulo actualizado')
              this.myForm.reset(this.myForm.value);
            } else {
              this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
            }
          }
        );
    }
  }
}
