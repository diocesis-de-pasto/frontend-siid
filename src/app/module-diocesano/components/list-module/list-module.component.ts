import { Component, OnInit, AfterViewInit, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { User } from 'src/app/user/user';
import { InjectTokenService } from 'src/app/sidebar/services/inject-token.service';
import { ModuleService } from '../../service/module.service';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { MatTableDataSource } from '@angular/material/table'
import { Module } from '../../interface/module';
import { CrudPermiss } from 'src/app/interface/crud-permiss';
declare const $: any;
@Component({
  selector: 'app-list-module',
  templateUrl: './list-module.component.html',
  styleUrls: ['./list-module.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ListModuleComponent implements OnInit, AfterViewInit {

  public reference: any[];
  public crudUser: CrudPermiss;
  public dataModule = new MatTableDataSource<Module>();
  public myForm: FormGroup = new FormGroup({});
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @Output() usuarioSeleccionado: EventEmitter<User>;
  public displayedColumns = [
    'nameModule',
    'typeModule',
    'refModule',
    'urlModule',
    'iconModule',
    'abbreviationModule',
    'description',
    'crud',
    'acciones'
  ];
  constructor(private moduleService: ModuleService,
    private errorHandlingService: ErrorHandlingService,
    private router: Router,
    private injectTokenService: InjectTokenService) {
    this.usuarioSeleccionado = new EventEmitter();
  }
  ngOnInit() {
    this.crudUser=  this.injectTokenService.permission('Administración de módulos');
    this.dataModule.sort = this.sort;
  }
  async viewModule(idModule: string) {
    this.router.navigate([`/session/module/view/${idModule}`]);
  }

  createModule() {

    this.router.navigate([`/session/module/create`]);

  }
  async deleteModule(id: number, modulo: string, element) {
    var petition = await this.errorHandlingService.question(`¿Eliminar módulo ${modulo}?`,
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {
      this.moduleService.deleteModule(id).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer(`Módulos ${modulo} borrado exitosamente`)
            this.dataModule.data = this.dataModule.data.filter(i => i !== element)
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      );
    }
  }
  async editModule(idModule: string) {
    this.router.navigate([`/session/module/edit/${idModule}`]);
  }
  referenceModuleName(referencia:number,name:string){
    for (let index = 0; index < this.reference.length; index++) {
      if(referencia==0){
        return name;
      }
      else if (Number(this.reference[index].idModule)===Number(referencia)){
        return this.reference[index].nameModule

      }
      
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataModule.filter = filterValue.trim().toLowerCase();
    if (this.dataModule.paginator) {
      this.dataModule.paginator.firstPage();
    }
  }
  async ngAfterViewInit() {
    await this.moduleService.listModule().subscribe(resp => {
      if (resp !== false) {
        this.dataModule.data = resp;
        this.reference=resp;
      }
    });
    this.dataModule.paginator = this.paginator;
  }
}