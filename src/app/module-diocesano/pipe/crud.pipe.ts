import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'crud'
})
export class CrudPipe implements PipeTransform {


  transform(value: string): string {

    if (value === 'C') {
      return 'Crear';

    } else if (value === 'R') {
      return 'Leer';

    } else if (value === 'U') {
      return 'Editar';

    } else if (value === 'D') {
      return 'Eliminar';

    }else{
      return '--'
    }
  }
}
