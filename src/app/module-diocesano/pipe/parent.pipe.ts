import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'parent'
})
export class ParentPipe implements PipeTransform {

  transform(value:string):string{
    
    if(value===('sub') ){
        return 'Padre';

    }else if(value===('child')){
        return 'Hijo';

    }else{
      return '-';
    }
  }
    
   
}
