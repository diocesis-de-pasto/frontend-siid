import { CrudPipe } from './crud.pipe';

describe('CrudPipe', () => {
  it('create an instance', () => {
    const pipe = new CrudPipe();
    expect(pipe).toBeTruthy();
  });


    //DEBES DE RETORNAR --
    it('Debe de retornar Crear',()=>{
      const pipeData = new CrudPipe();
      const result = pipeData.transform('C');
      expect(result).toEqual('Crear')
    })
    it('Debe de retornar Leer',()=>{
      const pipeData = new CrudPipe();
      const result = pipeData.transform('R');
      expect(result).toEqual('Leer')
    })
    it('Debe de retornar Editar',()=>{
      const pipeData = new CrudPipe();
      const result = pipeData.transform('U');
      expect(result).toEqual('Editar')
    })
    it('Debe de retornar Eliminar',()=>{
      const pipeData = new CrudPipe();
      const result = pipeData.transform('D');
      expect(result).toEqual('Eliminar')
    })
    it('Debe de retornar --',()=>{
      const pipeData = new CrudPipe();
      const result = pipeData.transform('');
      expect(result).toEqual('--')
    })
});
