import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ModuleService {
  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient) { }
  listModule() {
    const url = `${this.baseUrl}/module`;
    return this.http.get<any>(url, { observe: 'response' }).pipe(
      map(resp => {


        if (resp.status === 204) {
          return false;
        } else {
          return resp.body
        }


      }),
      catchError(err => of(false))
    );
  }
  listModulePrincipal() {
    const url = `${this.baseUrl}/module`;
    return this.http.get<any>(url, { observe: 'response' }).pipe(
      map(resp => {

        if (resp.status === 204) {
          return ([]);
        } else {
          var arrayFull: any[] = [];
          resp.body.forEach(function (element, index) {
            if (element.refModule === '0') {
              arrayFull.push({
                idModule: element.idModule,
                nameModule: element.nameModule,
              })
            }
          });
          return arrayFull;
        }

      }),
      catchError(err => of([]))
    );
  }
  deleteModule(idModule: number) {
    const url = `${this.baseUrl}/module/${idModule}`;
    return this.http.delete<any>(url).pipe(
      map(resp => {
        if (resp.status === 200) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => of(false))
    )
  }
  putModule(body: object, idModule: number) {
    const url = `${this.baseUrl}/module/${idModule}`;
    return this.http.put<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(err => of(false))
      );
  }
  getModule(idModule: string) {
    const url = `${this.baseUrl}/module/${idModule}`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {


        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
        
      }),
      catchError(err => of(false))
    );
  }
  postModule(
    nameModule: string,
    typeModule: string,
    refModule: number,
    urlModule: string,
    iconModule: string,
    abbreviationModule: string,
    description: string,
    crud: string
  ): Observable<any> {
    const url = `${this.baseUrl}/module`;
    const body = {
      nameModule,
      typeModule,
      refModule,
      urlModule,
      iconModule,
      abbreviationModule,
      description,
      crud
    };
    return this.http.post<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        })
        ,
        catchError(err => of(false))
      );
  }
  getModuleId(id: number) {
    const url = `${this.baseUrl}/module/${id}`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
      }),
      catchError(err => of(false))
    );
  }
}
