export interface Module {
    idModule: number,
    nameModule: string,
    typeModule: string,
    refModule: string,
    urlModule: string,
    iconModule:string,
    abbreviationModule: string,
    description: string,
    crud: string|null
}
