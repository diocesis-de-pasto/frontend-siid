import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { environment } from '../../../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class StaticsService {
  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient) { }

  listPrebiteros() {
    const url = `${this.baseUrl}/statistics/ordinationTime`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {

        if(resp.status===204){       
          return false;
        }else{

          return resp.body
        }
      }),
      catchError(err => of(false))

    )


  }
  listPriestAges() {
    const url = `${this.baseUrl}/statistics/getAges`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {

        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
      }),
      catchError(err => of(false))

    )


  }
  numberStudies() {
    const url = `${this.baseUrl}/statistics/studiesPriest`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
      }),
      catchError(err => of(false))

    )


  }
  serviceTime() {
    const url = `${this.baseUrl}/statistics/serviceTime`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
      }),
      catchError(err => of(false))

    )


  }













}
