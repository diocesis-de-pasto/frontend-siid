import { Component, ViewEncapsulation, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup } from '@angular/forms';
import { StaticsService } from '../service/statics.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  //grafica 1
  public single: any[];
  public multi: any[];
  public showXAxis = true;
  public showYAxis = true;
  public gradient = false;
  public showLegend = true;
  public showXAxisLabel = true;
  public xAxisLabel = 'Estudios';
  public showYAxisLabel = true;
  public yAxisLabel = 'No. Estudios';

  public colorScheme = {
    domain: ['rgb(168, 56, 93)', 'rgb(101, 235, 253)', 'rgb(153, 253, 208)', 'rgb(253, 214, 227)', 'rgb(254, 252, 250);', 'rgb(252, 238, 75) ', 'rgb(199, 39, 211)', 'rgb(0, 250, 83)', '#5AA454', '#A10A28', '#C7B42C']
  };
  //GRAFICA 2
  public single2: any[];
  public gradient2: boolean = true;
  public showLegend2: boolean = true;
  public showLabels: boolean = true;
  public isDoughnut: boolean = false;

  public colorScheme2 = {
    domain: ['rgb(165, 215, 198)', 'rgb(119, 148, 177)', 'rgb(191, 157, 118)', 'rgb(233, 148, 80)', 'rgb(216, 159, 89)', 'rgb(242, 223, 167)', '#AAAAAA']
  };

 
  //GRAFICA 3
  public single3: any[];
  public gradient3: boolean = true;
  public showLegend3: boolean = true;
  public showLabels3: boolean = true;
  public isDoughnut3: boolean = false;

  public colorScheme3 = {
    domain: ['rgb(165, 215, 198)', 'rgb(119, 148, 177)', 'rgb(191, 157, 118)', 'rgb(233, 148, 80)', 'rgb(216, 159, 89)', 'rgb(242, 223, 167)', 'rgb(144, 68, 151)','rgb(70, 100, 139)','rgb(78, 49, 165)','rgb(87, 70, 139)']
  };


///TABLA
totalPresbiteros:number=0;
totalSacerdotes:number=0;
borrar: boolean = false;
edit: boolean = true;
crudUser: any = {};
dataUser = new MatTableDataSource<any>();
@Output() usuarioSeleccionado: EventEmitter<any>;
@ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
@ViewChild(MatSort, { static: true }) sort: MatSort;
myForm: FormGroup = new FormGroup({});
displayedColumns = [
  "names",
  "surnames",
  "email",
  "identification",
  "time",
  "class",
 
];
single4: any[];
  constructor(private staticsService: StaticsService,



    
    ) {
 
  }

  async ngOnInit() {
    const respuesta1 = (await this.staticsService.numberStudies().toPromise())
    
    if (respuesta1!=false) {
      this.single =  this.parseInfoStudies(respuesta1);
    } else {
      this.single=[];
    }

    const respuesta2 = (await this.staticsService.listPrebiteros().toPromise())
    if (respuesta2!=false) {
      this.single2 =  this.parseInfo(respuesta2);
      this.single2.forEach(resp=>{
        this.totalPresbiteros+=resp.value;
  
        
      })
    } else {
      this.single=[];
    }


    const respuesta3 = (await this.staticsService.listPriestAges().toPromise())

    if (respuesta3!=false) {
      this.single3 =  this.parseInfo(respuesta3);
      this.single3.forEach(resp=>{
        this.totalSacerdotes+=resp.value;
  
        
      })
    } else {
      this.single=[];
    }

    const respuesta4 = (await this.staticsService.serviceTime().toPromise())
    if (respuesta4!=false) {
      this.dataUser.data =    respuesta4;
      this.dataUser.paginator = this.paginator;
    } else {
      this.dataUser.data =    [];
      this.dataUser.paginator = this.paginator;
    }     


  


  }
  parseInfoStudies(resp: any) {

    var estudios = [
      {
        name: 'Primarios',
        value: 20
      },
      {
        name: 'Secundarios',
        value: 20
      },
      {
        name: 'Eclesiasticos',
        value: 20
      },
      {
        name: 'Universitario',
        value: 20
      },
      {
        name: 'Pregado',
        value: 20
      },
      {
        name: 'Postgrado',
        value: 20
      },
      {
        name: 'Diplomado',
        value: 20
      },
      {
        name: 'Curso',
        value: 20
      },
      {
        name: 'Técnico',
        value: 20
      },
      {
        name: 'Tecnológico',
        value: 20
      },
      {
        name: 'Seminario',
        value: 20
      }
    ]
    var valuesEstudios: number[] = Object.values(resp);

    
    for (let index = 0; index < estudios.length; index++) {
      estudios[index].value = valuesEstudios[index]

    }



    return estudios;

  }
  parseInfo(resp: any) {

    var arrayTransform = [];
    for (let prop in resp) {
      arrayTransform.push(
        {
          name: `${prop} años`,
          value: Number(`${resp[prop]}`)
        }
      )
    }



    return arrayTransform;

  }







 




  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataUser.filter = filterValue.trim().toLowerCase();

    if (this.dataUser.paginator) {
      this.dataUser.paginator.firstPage();
    }
  }



    //errors
    campoNoValido(campo: string) {
      return this.myForm.get(campo)?.invalid
        && this.myForm.get(campo)?.touched
    }


}
