import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MdModule } from '../md/md.module';
import { MaterialModule } from '../app.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { NameEmpty } from './pipe/nameEmptyu.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './components/statics/home/home.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { YearsPipe } from './pipe/years.pipe';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(DashboardRoutes),
        MdModule, 
        MaterialModule,
        FormsModule,   
        ReactiveFormsModule,
        NgxChartsModule
    ],
    declarations: [DashboardComponent,NameEmpty,HomeComponent, YearsPipe]
})
export class DashboardModule {}
