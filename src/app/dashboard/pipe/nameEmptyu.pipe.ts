import { LowerCasePipe, UpperCasePipe } from "@angular/common";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name:'nameEmpty'
})
export class NameEmpty implements PipeTransform {

    transform(value:string,name:string):string{
        
        if(value===('') || value===(undefined) || value===(null)){
            return name;

        }else{
            return value;
        }
      
        
        
       
    }

}