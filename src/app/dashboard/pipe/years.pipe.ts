import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'years'
})
export class YearsPipe implements PipeTransform {


  transform(value:number):string{
    

    if(value===1){
        return  value+' año';

    }else{
      return value+ ' años';
    }
  
    
    
   
}

}
