import { YearsPipe } from './years.pipe';

describe('YearsPipe', () => {
  it('create an instance', () => {
    const pipe = new YearsPipe();
    expect(pipe).toBeTruthy();
  });

  it('Debe de retornar "año"',()=>{
    const pipeData = new YearsPipe();
    const result = pipeData.transform(1);
    expect(result).toEqual(1+' año')
  });
  it('Debe de retornar "años"',()=>{
    const pipeData = new YearsPipe();
    const result = pipeData.transform(2);
    expect(result).toEqual(2+' años')
  });
});
