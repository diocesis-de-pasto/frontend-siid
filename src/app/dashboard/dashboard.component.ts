import { Component, OnInit } from "@angular/core";

import { User } from "../user/user";
import { Person } from "../profile/interface/data";
import { GlobalService } from "../service/global.service";

import { PersonalService } from "../shared/components-user/components/personal-information/personal.service";
import { InjectTokenService } from '../sidebar/services/inject-token.service';
import { UserService } from 'src/app/shared/components-user/components/user-information/user.service';
import { CrudPermiss } from "../interface/crud-permiss";
import { ErrorHandlingService } from '../service/error-handling.service';

declare const $: any;
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {
  public urlImg = "/assets/img/userImg.png";
  public imgQr: any;
  public url = "";
  public user: User;
  public dataPerson: Person;
  public isShowQR: boolean = true;
  public textShowQRCode: string;
  public crudStatistics:CrudPermiss;
  constructor(
    private personalService: PersonalService,
    private userService: UserService,
    private globalService: GlobalService,
    private errorHandlingService: ErrorHandlingService,
    private injectTokenService: InjectTokenService
  ) {
    this.variablesInit();
    this.showQRCode();
  }


   ngOnInit(): void {

    this.crudStatistics=  this.injectTokenService.permission('Estadística');

    this.globalService.imgQr().subscribe((resp) => {
      if (resp !== false) {
        this.imgQr = resp.qr;
      }
    });
    this.usuario();
    this.getPersonalImg();
    this.getPersonal();
  }

  getPersonal() {
    this.personalService
      .getPersonal(String(this.globalService.userId))
      .subscribe((resp) => {
        if (resp.idDataPerson) {
          this.dataPerson = resp;
          if (this.dataPerson.photo === null) {
            this.urlImg = "/assets/img/userImg.png";
          }
        }
      });
  }

  usuario() {
    this.userService.getUserId(this.globalService.userId).subscribe((resp) => {
      this.user = resp || "";
    });
  }
  carnet() {
    this.userService.getCarnetId(this.globalService.userId).subscribe(
      resp => {

        if (resp !== false) {
          var url = window.URL.createObjectURL(resp);
          window.open(url);
        } else {
          this.errorHandlingService.negativeAnswer('El usuario no tiene cargada una imagen')
        }

      }
    )
  }
  getPersonalImg() {
    this.personalService
      .getPersonalImage(String(this.globalService.userId))
      .subscribe((resp) => {
        if (resp !== false) {
          if (resp.image !== "") {
            this.urlImg = resp.image;
          }
        }
      });
  }

  variablesInit() {
    this.user = {
      idUser: null,
      nickname: "",
      email: "",
      emailCorporate: "",
      state: "",
      documentId: "",
      nameTypeUser: "",
      documentType: "",
    };

    this.dataPerson = {
      firstName: "",
      secondName: "",
      firstSurname: "",
      secondSurname: "",
      documentType: "",
      documentId: "",
      cityOfBirth: "",
      dateOfBirth: "",
      bloodType: "",
      contactNumberOne: "",
      contactNumberTwo: "",
      email: "",
      photo: "",
    };
  }

  showQRCode() {
    if (this.isShowQR) {
      this.isShowQR = false;
      this.textShowQRCode = 'Ver Mi QR';
    } else {
      this.isShowQR = true;
      this.textShowQRCode = 'Ocultar Mi QR';
    }
  }
}
