import { Injectable } from '@angular/core';
import Swal, { SweetAlertIcon, SweetAlertPosition } from 'sweetalert2'
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService {
  constructor() { }
  question(
    htmlMsg: string = "",
    confirmButtonMsg: string = "",
    cancelButtonMsg: string = "",
    confirmButtonTextMsg: string = "",
    cancelButtonTextMsg: string = "",
    iconMsgDio: SweetAlertIcon = 'error',
    titleMsg: string = "",
  ): any {
    const swalWithBootstrapButtons = Swal.mixin(
      {
        customClass: {
          cancelButton: cancelButtonMsg,
          confirmButton: confirmButtonMsg,
        },
        buttonsStyling: false
      })
    return swalWithBootstrapButtons.fire(
      {
        title: titleMsg,
        html: htmlMsg,
        icon: iconMsgDio,
        showCancelButton: true,
        showConfirmButton: true,
        cancelButtonText: cancelButtonTextMsg,
        confirmButtonText: confirmButtonTextMsg,
        reverseButtons: true
      }
    ).then(result => {
      if (result.isConfirmed) {
        return true;
      }
      return false;
    })
  }
  positiveAnswer(title: string) {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: title,
      showConfirmButton: false,
      timer: 2500
    })
  }
  negativeAnswer(title: string) {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: title,
      confirmButtonText: 'Aceptar',
      customClass: {
        popup: 'format-pre'
      },
      timer: 4500
    });
  }
  negativeAnswerPermanent(title: string) {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: title,
      confirmButtonText: 'Aceptar',
      customClass: {
        popup: 'format-pre'
      }
    });
  }



  errorsEmail(err:any){

    if (err?.required) {
      return 'Email es obligatorio'
    } else if (err?.pattern) {
      return 'El valor ingresado no tiene formato de correo'
    }

  }
  name(err:any){


    if (err.required) {
      return 'es obligatorio'
    } else if (err.pattern) {

      
      if(err.pattern.requiredPattern ==="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$"){
        return 'unicamente debe contener texto'
      }
      if(err.pattern.requiredPattern ==="^[0-9]+$"){
        return 'unicamente debe contener numeros'
      }
      if(err.pattern.requiredPattern ==="^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"){
        return 'formato incorrecto, example@gmail.com'
      }
 
    }
     else if (err.maxlength) {
      return `'debe tener maximo ${err.maxlength.requiredLength} caracteres'`
    }
 
    return ''
  }



}
