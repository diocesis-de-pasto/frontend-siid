import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class NameService {
  // public nameUser: string = "^[a-zA-Z]{4}[0-9]{6}";
  public emailPattern: string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";
  public namePattern: string = "^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$";
  public numberPattern: string = "^[0-9]+$";
  constructor() { }
}
