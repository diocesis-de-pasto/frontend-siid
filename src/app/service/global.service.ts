import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { objectUser } from '../sidebar/services/inject-token.service';
import jwt_decode from 'jwt-decode';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  private baseUrl: string = environment.baseUrl;
  constructor(
    private http:HttpClient
  ){}
  get userId(): number {
    if(localStorage.getItem('x-tq')){
      let objectUser: objectUser = jwt_decode(localStorage.getItem('x-tq'));
      return  objectUser.sub;
    }else{
      return null;
    }
  }
  get userEmail(): string {
    if(localStorage.getItem('x-tq')){
      let objectUser: objectUser = jwt_decode(localStorage.getItem('x-tq'));
      return  objectUser.email;
    }else{
      return null;
    }
  }
  get indenficationUser(): string {
    if(localStorage.getItem('x-tq')){
      let objectUser: objectUser = jwt_decode(localStorage.getItem('x-tq'));
      return  objectUser.email;
    }else{
      return null;
    }
  }

  imgQr(){
    const url = `${this.baseUrl}/qr/${this.userId}`;   
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if (resp.status===204 ) {
          return false;
        } else {
          return resp.body;
        }
      })
      ,
      catchError(err => of(false))
    );
  }
}
