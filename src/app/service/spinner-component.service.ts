import { Injectable, Component } from '@angular/core';
import { Event, NavigationCancel, NavigationEnd, NavigationStart, Router, NavigationError, ResolveEnd } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
@Component({
  template: `
  <div class="overlay" *ngIf="loading===true">
        <div class="loadingio-spinner-rolling-zvsd6sfg6e"><div class="ldio-ije64jmhzm">
        </div></div>
  </div>
  `
})
export class SpinnerComponentService {
  loading = false;
  constructor(private router: Router) { 
    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart:{
          this.loading = true;
          break;
        }
        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: 
        case event instanceof   ResolveEnd:
     {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
  }
}
