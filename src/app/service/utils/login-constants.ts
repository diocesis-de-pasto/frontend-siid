export class LoginConstants {
    public static SECTION_LEFT = {
        TEXT_PRIMARY: "Sistema Integrado de Información Diocesana",
        TEXT_SECONDARY: "Diócesis de Pasto",
        IMAGE: "/assets/img/logo-diocesis.png",
    }
    public static SECTION_RIGHT = {
        WELCOME_TITLE: "Bienvenid@",
        WELCOME_MSG: "Para iniciar sesión, por favor ingrese su usuario y contraseña",
        EMAIL: "info@diocesisdepasto.org",
        SOCIAL_NETWORK: "Siguenos en nuestras redes sociales"
    }
    public static SOCIAL_NETWORK = {
        FACEBOOK: "https://www.facebook.com/DiocesisDePastoOficial",
        INSTRAGRAM: "https://www.instagram.com/DiocesisDePastoOficial/",
        TWITTER: "https://twitter.com/Diocesis_Pasto"
    }

    public static SECTION_REGISTER = {
        TITLE: "Registro",
        TEXT_INFO: "Por favor ingrese su documento de identidad y un correo electrónico al que pueda acceder."
    }
}