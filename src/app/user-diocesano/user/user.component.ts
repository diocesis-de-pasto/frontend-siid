import { HttpBackend, HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { GlobalService } from '../../service/global.service';
import { QrUser } from '../interface/qr-user';
import { QrService } from '../service/qr.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public add:boolean=true;
  public numberSlice:number=2;
  public start:number=0;
  public end:number=1;
  public text:string='Ver mas ...';
  public time = new Date();
  public rxTime = new Date();
  public intervalId: any;
  public subscription: Subscription;
  public idUser: string;
  private httpClient: HttpClient;
  public   userDate: QrUser = {

    documentId: "",
    documentType: "",
    nameInstitution: "",
    nameTypeUser: "",
    nameUser: "",
    photo: "/assets/img/user2.png",
    dataPastoral:[
      {
      
        idDataPastoral: 0,
        pastoralService:'',
        assignedPlace:'',
        appointmentDate: null,
        serviceStartDate: null,
        serviceEndDate: null,
        timeRemaining: null,
  
      }
    ]

  };;
  constructor(private activatedRoute: ActivatedRoute,
    private qrService: QrService,
    private router: Router,
    httpBackend: HttpBackend) {
    this.httpClient = new HttpClient(httpBackend);
  }









  verificate(){
    if(this.text==='Ver mas ...'){
      this.text='Ver menos';
      this.end=         this.userDate.dataPastoral.length;
    }else if(this.text==='Ver menos'){
      this.text='Ver mas ...';
       this.end=1;


    }
  }
  ngOnInit() {
    this.idUser = this.activatedRoute.snapshot.params.idQr;
    
    if (this.idUser) {

      this.qrService.returnContentQr(this.idUser).subscribe(
        resp => {
   
          
          
          if (resp.documentId){
            
            this.userDate = resp;
            if(this.userDate.photo===''){
              this.userDate.photo='/assets/img/user2.png';
            }

          }
        }
      )


    }












    // Using Basic Interval
    this.intervalId = setInterval(() => {
      this.time = new Date();
    }, 1000);

    // Using RxJS Timer
    this.subscription = timer(0, 1000)
      .pipe(
        map(() => new Date()),
        share()
      )
      .subscribe(time => {
        this.rxTime = time;
      });
  }

  ngOnDestroy() {
    clearInterval(this.intervalId);
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
