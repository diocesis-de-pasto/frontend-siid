import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';

const routes: Routes = [  {
  path: '',
  children: [{
      path: '',
      component: UserComponent
  }] 

},{

  path: '',
  children: [
    {
      path: ':idQr',
      component: UserComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserDiocesanoRoutingModule { }
