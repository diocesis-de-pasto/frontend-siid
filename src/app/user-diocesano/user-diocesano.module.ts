import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserDiocesanoRoutingModule } from './user-diocesano-routing.module';
import { UserComponent } from './user/user.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    UserComponent

  ],
  imports: [
    CommonModule,
    UserDiocesanoRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
    
  ]
})
export class UserDiocesanoModule { }
