export interface QrUser {
 
        documentId: string,
        documentType:string,
        nameInstitution:string,
        nameTypeUser: string,
        nameUser: string,
        photo: string|null,
        dataPastoral:Pastoral[]
 
}
export interface Pastoral {
 
        idDataPastoral: number,
        pastoralService:string,
        assignedPlace:string,
        appointmentDate: Date,
        serviceStartDate: Date,
        serviceEndDate: Date,
        timeRemaining: number
 
}
