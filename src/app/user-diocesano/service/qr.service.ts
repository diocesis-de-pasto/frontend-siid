import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class QrService {
  private baseUrl: string = environment.baseUrl;

  constructor(
    private httpClient: HttpClient,
               private router: Router,
               httpBackend: HttpBackend
             ) { this.httpClient = new HttpClient(httpBackend);}

  returnContentQr(qr:string){
    const url = `${this.baseUrl}/qr/dataQR/${qr}`;   
  

    return this.httpClient.get<any>(url)
    .pipe(

      map(resp=>{
        
        if(resp.documentId){
          
          return resp;
        }else{
          false;
        }
      }),
   
    );
  




    
    
  }

}
