
import { StatePipe } from './state.pipe';

describe('Pipe state ', () => {
  it('create an instance', () => {
    const pipe = new StatePipe();
    expect(pipe).toBeTruthy();
  });
  //DEBES DE RETORNAR --
  it('Debe de retornar "Activo" si el valor es enable',()=>{
      const pipeState = new StatePipe();
      const result = pipeState.transform('enable');
      expect(result).toEqual('Activo')
  })
  it('Debe de retornar "Inactivo" si el valor es disable',()=>{
    const pipeState = new StatePipe();
    const result = pipeState.transform('desable');
    expect(result).toEqual('Inactivo')
  })
});
