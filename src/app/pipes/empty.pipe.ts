import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'emptyValueDio'
})
export class EmptyPipe implements PipeTransform {
  transform(value: string): string {
    if (value === "" || value===null ||value===undefined) {
      return " -- ";
    }else{
      return value;
    }
  }
}
