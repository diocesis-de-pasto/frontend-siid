
import { EmptyPipe } from './empty.pipe';

describe('Pipe empty ', () => {
  it('create an instance', () => {
    const pipe = new EmptyPipe();
    expect(pipe).toBeTruthy();
  });
  //DEBES DE RETORNAR --
  it('Debe retornar "--"',()=>{
      const pipeData = new EmptyPipe();
      const result = pipeData.transform('');
      expect(result).toEqual(' -- ')
  })

});
