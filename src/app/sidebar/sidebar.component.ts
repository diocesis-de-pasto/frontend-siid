import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';

import { AuthService } from '../auth/services/auth.service';
import { InjectTokenService } from './services/inject-token.service';

import { GlobalService } from '../service/global.service';
import { Person } from '../profile/interface/data';
import { UserService } from '../shared/components-user/components/user-information/user.service';
import { PersonalService } from '../shared/components-user/components/personal-information/personal.service';

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export var ROUTES: RouteInfo[] = [];


@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {


    dataPerson: Person = {
        firstName: "",
        secondName: "",
        firstSurname: "",
        secondSurname: "",
        documentType: "",
        documentId: "",
        cityOfBirth: "",
        dateOfBirth: "",
        bloodType: "",
        contactNumberOne: "",
        contactNumberTwo: "",
        email: "",
        photo: '',
      };
    idIdentification:string;
      urlImg = "/assets/img/faces/userPrueba.png";
    constructor(

        private authService: AuthService,
        private injectTokenService: InjectTokenService,
        private personalService: PersonalService,
        private globalService:GlobalService,
        private  userService:UserService) {

    }
    public menuItems: any[];
    ps: any;
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    async logout() {


            this.authService.logout();

        

    }

    ngOnInit() {

        this.authService.movimiento();
        this.injectTokenService.routes()
        ROUTES = this.injectTokenService.routesInSidebar();
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            this.ps = new PerfectScrollbar(elemSidebar);
        }
        this.nameUser();
        this.idUser();
        this.getPersonalImg();
    }
    nameUser(){
        this.personalService.getPersonal(String(this.globalService.userId)).subscribe(
            resp=>{
                if(resp!==false){
                    this.dataPerson=resp;
                }
            }
        )
    }
    idUser(){
        this.userService.getUser(String(this.globalService.userId)).subscribe(
            resp=>{
                
                
                if(resp!==false){
                    this.idIdentification=resp.documentId;
                }
            }
        )
    }
    getPersonalImg() {

        this.personalService.getPersonalImage(String(this.globalService.userId)).subscribe(
          resp => {
            if (resp !== false) {
              if(resp.image!==''){
                this.urlImg = resp.image
              }
            }
          }
        )
    
      }
    updatePS(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            this.ps.update();
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }  
        return bool;
      

    }
}
