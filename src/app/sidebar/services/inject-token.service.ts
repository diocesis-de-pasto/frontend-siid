import { Injectable } from '@angular/core';


import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';

import jwt_decode from "jwt-decode";
import { Base64 } from 'js-base64';

import { map, tap } from "rxjs/operators"; //Operador que permite mutar la respuesta en el   
import { UserService } from 'src/app/shared/components-user/components/user-information/user.service';
import { Crud } from 'src/app/permissions/interface/permissions';
import { CrudPermiss } from 'src/app/interface/crud-permiss';
export interface objectUser {
  email: string;
  sub: number;
  modules: string;
  iat: number;
  exp: number;
}
export interface moduloUsuario {
  path: string;
  title: number;
  type: string;
  icontype: number;
  collapse: string;
}
export interface children {
  path: string;
  title: string;
  ab: string;

}

@Injectable({
  providedIn: 'root'
})
export class InjectTokenService {
  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient, private router: Router,
    private userService: UserService) {
  }



  componentAcces(ruta: string): boolean {
    var array = this.routes();
    var available = false;
    array.filter(resp => {
      if (ruta.indexOf(resp) === 0) {
        available = true;
        return true;
      }
    })
    return available;
  }
  routes() {
    var containerRoutes = [];
    var objectUsere: any = jwt_decode(localStorage.getItem('x-tq'))

    var modules = (Base64.decode(objectUsere.modules));

    objectUsere.routes.forEach(resp => {
      containerRoutes.push(resp.link)
      resp.routesChild.forEach(resp => {
        containerRoutes.push(resp)
      });
    });
    containerRoutes.push('/session/dashboard')
    containerRoutes.push('/session/profile')
    containerRoutes.push('/session/newRoute')

    return containerRoutes;


  }
  routesInSidebar() {


    var objectUser: any = jwt_decode(localStorage.getItem('x-tq'))
    var modules = (Base64.decode(objectUser.modules));
    //console.log(modules);
    var arrayEnd = [];
    arrayEnd.push({
      path: '/session/dashboard',
      title: 'Dashboard',
      type: 'link',
      icontype: 'picture_in_picture'
    })
    objectUser.routes.forEach(resp => {
      arrayEnd.push({
        path: `${resp.link}`,
        title: resp.title,
        type: resp.type,
        icontype: resp.icon
      })
    });
    return arrayEnd;

  }
  routesValidation() {


    let objectUsere: objectUser = jwt_decode(localStorage.getItem('x-tq'))
    var editUser = false;
    var modules = (Base64.decode(objectUsere.modules));



    var arrayEnd2 = JSON.parse(modules);



    var arrayEnd = [];
    arrayEnd.push({
      path: '/dashboard',
      permissions: [1, 1, 1, 1]
    })

    arrayEnd2.map(function (table) {




      if (table.path === 'user/' || table.path === 'module/' || table.path === 'role/') {
        arrayEnd.push({
          path: table.path,
          permissions: table.permissions,
        })
      }
    });

    return arrayEnd;

  }
  permission(name: string) {


    let objectUsere: objectUser = jwt_decode(localStorage.getItem('x-tq'))
    var modules = (Base64.decode(objectUsere.modules));

    var arrayEnd2 = JSON.parse(modules);
    var element: CrudPermiss = {
      admin: 0,
      create: 0,
      read: 0,
      update: 0,
      delete: 0
    }





    arrayEnd2.map(function (table) {


      if (name === table.title) {

        element.admin = table.permissions[0];
        element.create = table.permissions[1];
        element.read = table.permissions[2];
        element.update = table.permissions[3];
        element.delete = table.permissions[4];

      }



    });





    return element;

  }

  firsteTimeUser() {
    let objectUser: objectUser = jwt_decode(localStorage.getItem('x-tq'));
    return this.userService.getUserId(objectUser.sub).pipe(
      map(resp => {
        if (resp.numberOfAdmissions === 0) {
          return false;
        }
        else {
          return true;
        }

      }),


    )




  }
}
