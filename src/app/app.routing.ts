import { Routes, CanActivate, RouterModule } from '@angular/router';
import { ValidateTokenGuard } from './auth/guards/validate-token.guard';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { ValidateComponentsGuard } from './auth/guards/validate-components.guard';
import { PrimaryGuard } from './password/guard/primary.guard';

export const AppRoutes: Routes = [

    {
        path: '',
        redirectTo: '/login',
       
        pathMatch: 'full',
    },
    {
        path: 'session',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
                , canActivateChild:[PrimaryGuard]
            }
            ,{
                path: 'user',
                loadChildren: () => import('./user/user.module').then(m => m.UserModule),
                 canActivateChild:[PrimaryGuard]
            } ,{
                path: 'module',
                loadChildren: () => import('./module-diocesano/module-diocesano.module').then(m => m.ModuleDiocesanoModule),
                 canActivateChild:[PrimaryGuard]
            }
            ,{
                path: 'profile',
                loadChildren: () => import('./profile/profile.module').then(m => m.UserDataModule)
                , canActivateChild:[PrimaryGuard]
            },{
                path: 'role',
                loadChildren: () => import('./role-diocesano/role-diocesano.module').then(m => m.RoleDiocesanoModule)
                 , canActivateChild:[PrimaryGuard]
            }
            ,{
                path: 'password',
                loadChildren: () => import('./password/password.module').then(m => m.PasswordModule)
            }
            ,{
                path: 'permissions',
                loadChildren: () => import('./permissions/permissions.module').then(m => m.PermissionsModule)
                , canActivateChild:[PrimaryGuard]
            }
          
            ,
            {
                path: '**',
                redirectTo: 'dashboard',
                
            }
        ] 
        , canActivateChild:[ValidateTokenGuard,ValidateComponentsGuard]
    }
    , {
        path: 'login',
        children: [{
          path: '',
          loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
        }]
        

      },
      {
        path: 'register',
        loadChildren: () => import('./register-user/register-user.module').then(m => m.RegisterUserModule)
      }
      ,{
        path: 'qr',
        loadChildren: () => import('./user-diocesano/user-diocesano.module').then(m => m.UserDiocesanoModule)
        

      },{
        path: 'recovery-password',
        loadChildren: () => import('./recovery-password/recovery-password.module').then(m => m.RecoveryPasswordModule)
      },
   




];
