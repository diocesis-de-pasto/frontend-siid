import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonalInformationComponent } from './components/personal-information/personal-information.component';
import { MenisterialInformationComponent } from './components/menisterial-information/menisterial-information.component';
import { AcademicInformationComponent } from './components/academic-information/academic-information.component';
import { PastoralInformationComponent } from './components/pastoral-information/pastoral-information.component';
import { RecognitionInformationComponent } from './components/recognition-information/recognition-information.component';
import { FamilyContactInformationComponent } from './components/family-contact-information/family-contact-information.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../app.module';
import { UserInformationComponent } from './components/user-information/user-information.component';
import { GenerPipe } from './gener.pipe';



@NgModule({
  declarations: [
    PersonalInformationComponent,
    MenisterialInformationComponent,
    AcademicInformationComponent,
    PastoralInformationComponent,
    RecognitionInformationComponent,
    FamilyContactInformationComponent,
    UserInformationComponent,
    GenerPipe,

  ],
  imports: [
    CommonModule,
    FormsModule,   
     ReactiveFormsModule,
     MaterialModule,
     
  ],
  exports:[
    PersonalInformationComponent,
    MenisterialInformationComponent,
    AcademicInformationComponent,
    PastoralInformationComponent,
    RecognitionInformationComponent,
    FamilyContactInformationComponent,
    UserInformationComponent
  ]
})
export class ComponentsUserModule { }
