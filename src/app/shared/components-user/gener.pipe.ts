import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gener'
})
export class GenerPipe implements PipeTransform {

  transform(value: string): string {
    if (value === "M") {
      
      return "Masculino";
    }else if(value === "F")
    {
      return "Femenino";
    }
  }

}
