
export interface Academic {
    idDataAcademy: number,
    levelOfStudy: string,
    institutionName: string,
    titleReceived: string,
    year: Date,
    lastYearStudied: string,
    culminationDate: Date,
    projectedGradeDate: Date,
    state: string,
    numberOfHours:number ,
    city: string,
    department: string,
    country: string,
}
export interface FamilyContact {
}
export interface Menisterial {
    diaconalOrdinationDate: Date,
    priestlyOrdinationDate: Date,
    episcopalOrdinationDate: Date,
    
}
export interface Pastoral {
    decreeNumber:string
    idDataPastoral:number;
    pastoralService: string;
    assignedPlace: string;
    appointmentDate: Date;
    canonicalDecreeDate: Date,
    serviceStartDate: Date,
    serviceEndDate: Date,


}
export interface Person {
    firstName:string,
    secondName: string,
    firstSurname: string,
    secondSurname: string,
    documentType:string,
    documentId: string,
    cityOfBirth: string,
    dateOfBirth:string,
    bloodType: string,
    contactNumberOne: string,
    contactNumberTwo: string,
    email: string,

}
export interface Recognition {
    idRecognition:number,
    recognitionMotive:string,
    recognitionDate: Date,
    entityThatIssuesRecognition: string ,
}
