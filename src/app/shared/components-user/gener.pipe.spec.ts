
import { GenerPipe } from './gener.pipe';


describe('Pipe genero', () => {
  it('create an instance', () => {
    const pipe = new GenerPipe();
    expect(pipe).toBeTruthy();
  });
  //DEBES DE RETORNAR --
  it('Debe de retornar Masculino si es M', () => {
    const gener = new GenerPipe();
    const result = gener.transform('M');
    expect(result).toEqual('Masculino')
  })
  it('Debe de retornar Femenino si es F', () => {
    const gener = new GenerPipe();
    const result = gener.transform('F');
    expect(result).toEqual('Femenino')
  })
});
