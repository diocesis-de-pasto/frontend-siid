import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudPermiss } from 'src/app/interface/crud-permiss';

import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { GlobalService } from 'src/app/service/global.service';
import { getUpdatedValue, verifateUnit } from '../functions'
import { MenisterialService } from './menisterial.service';
@Component({
  selector: 'app-menisterial-information',
  templateUrl: './menisterial-information.component.html',
  styleUrls: ['../../../../css/formulario.css'],
})
export class MenisterialInformationComponent implements OnInit {
  public selectedItem: string;
  public first: boolean = false;
  public delete: boolean = false;
  public update: boolean = false;
  public create: boolean = false;
  public id: string;
  public myForm: FormGroup = new FormGroup({});
  public diaconalOrdinationDate: Date = new Date();
  public priestlyOrdinationDate: Date = new Date();
  public episcopalOrdinationDate: Date = new Date();

  @Input() idchild: string;
  @Input() permissCrud:CrudPermiss;
  @Input() view: boolean =false;

  constructor(private fb: FormBuilder,

    private menisterialService: MenisterialService,
    private globalService: GlobalService
    , private errorHandlingService: ErrorHandlingService,
    ) {
    }
    ngOnInit() {
      if (this.idchild) {
        this.id = this.idchild.toString();
      } else {
        this.id = this.globalService.userId.toString();
      }
      this.myForm = this.fb.group(
        {
          diaconalOrdinationDate: [, [Validators.required]],
          priestlyOrdinationDate: [, [Validators.required]],
          episcopalOrdinationDate: [],
        });
      this.getMenisterial();
    }
  getMenisterial(){
    this.menisterialService.getMenisterial(     this.id).subscribe(
      resp => {
        if (resp !== false) {
          this.delete = true;
          this.update = true;
          this.create = false;
          this.myForm.controls['diaconalOrdinationDate'].setValue(resp.diaconalOrdinationDate);
          this.myForm.controls['priestlyOrdinationDate'].setValue(resp.priestlyOrdinationDate);
          this.myForm.controls['episcopalOrdinationDate'].setValue(resp.episcopalOrdinationDate);
        }else{
          this.delete = false;
          this.update = false;
          this.create = true;
        }
      }
    )
  }
  async deleteMinisterial() {
    var petition = await this.errorHandlingService.question('¿Eliminar datos ministeriales?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {
      this.menisterialService.deleteMenisterial(this.id).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Información ministerial eliminada')
            this.myForm.reset(this.myForm.reset);
            this.delete = false;
            this.update = false;
            this.create = true;
          }else{
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      )
    }
  }
  verificateMenisterial(){
    return verifateUnit(this.permissCrud.create,this.permissCrud.update, this.create)
   } 
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  async menisterialInformation() {
    if (this.myForm.invalid === true) {
      this.myForm.markAllAsTouched();
      this.errorHandlingService.negativeAnswer('Ingrese los datos requeridos')
    }else{
      this.menisterialService.postMenisterial(
        this.myForm.value.diaconalOrdinationDate,
        this.myForm.value.priestlyOrdinationDate,
        this.myForm.value.episcopalOrdinationDate,
        Number(this.id) 
      ).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Información ministerial actualizada')
            this.menisterialService.getMenisterial(this.id).subscribe(
              resp => {
                if (resp !== false) {
                  this.myForm.controls['diaconalOrdinationDate'].setValue(resp.diaconalOrdinationDate);
                  this.myForm.controls['priestlyOrdinationDate'].setValue(resp.priestlyOrdinationDate);
                  this.myForm.controls['episcopalOrdinationDate'].setValue(resp.episcopalOrdinationDate);
                  this.delete = true;
                  this.update = true;
                  this.create = false;
                }
              }
            )
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      );
    }
  }
  async updateMenisterial() {
    var change = getUpdatedValue(this.myForm);
    this.menisterialService.putMenisterial(change, Number(this.id))
      .subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Información ministerial actualizada')
            this.myForm.reset(this.myForm.value);
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      )
  }

  errorMsgForm(nameFormValue: string, name: string): string {
    const errors = this.myForm.get(nameFormValue)?.errors;
    if (errors) {

      return ` ${name} ${this.errorHandlingService.name(errors)}`;
    }
  }
}
