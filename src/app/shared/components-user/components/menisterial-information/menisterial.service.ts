import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { GlobalService } from 'src/app/service/global.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MenisterialService {

  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient, private globalService: GlobalService) { }


  postMenisterial(
    diaconalOrdinationDate: Date,
    priestlyOrdinationDate: Date,
    episcopalOrdinationDate: Date | any,
    id: number
  ): Observable<any> {
    var user = {
      idUser: id
    };
    const url = `${this.baseUrl}/ministerial-information`;
    const body = { diaconalOrdinationDate, priestlyOrdinationDate, episcopalOrdinationDate, user };
    return this.http.post<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(err => of(false))
      );
  }
  putMenisterial(body: object, id: number): Observable<any> {
    const url = `${this.baseUrl}/ministerial-information`;
    var userMenisterial = {
      user: {
        idUser: id
      }
    };
    var petition = Object.assign(body, userMenisterial);
    return this.http.put<any>(url, petition)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(err => of(false))
      );
  }
  getMenisterial(id: string): Observable<any> {
    const url = `${this.baseUrl}/ministerial-information/${id}`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body;
        }
      }),
      catchError(err => of(false))
    );
  }
  deleteMenisterial(id: string): Observable<any> {
    const url = `${this.baseUrl}/ministerial-information/${id}`;
    return this.http.delete<any>(url).pipe(
      map(resp => {
        if (resp.status === 200) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => of(false))
    );
  }


}
