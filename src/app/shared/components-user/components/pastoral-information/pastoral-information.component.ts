import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Pastoral } from '../../interface/data';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { GlobalService } from 'src/app/service/global.service';
import { verifate } from '../functions';
import { NameService } from '../../../../service/name.service';
import { PastoralService } from './pastoral.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';




@Component({
  selector: 'app-pastoral-information',
  templateUrl: './pastoral-information.component.html',
  styleUrls: ['../../../../css/formulario.css'],
})


export class PastoralInformationComponent implements OnInit {

  selectedItem: string;
  inforAcademic: Pastoral[] = [];
  invalid: boolean = false;
  delete: boolean = false;
  myForm: FormGroup = new FormGroup({});
  create: boolean = false;
  update: boolean = false;
  add:boolean=false;
  @Input() idchild: string;
  @Input() permissCrud: CrudPermiss;
  @Input() view: boolean = false;
  id: string;



  constructor(
    private fb: FormBuilder,
    private pastoralService: PastoralService,
    private errorHandlingService: ErrorHandlingService,
    private formBuilder: FormBuilder,
    private globalService: GlobalService,
    private nameService:NameService
  ) {
  }


  ngOnInit() {
    if (this.idchild) {
      this.id = this.idchild.toString();
    } else {
      this.id = this.globalService.userId.toString();
    }
    this.getPastoral();
    this.myForm = this.fb.group({
      pastoralArray: this.formBuilder.array([]),
    });
  }
  servicePastoral = [
    "Parroco", "Vicario", "Capellan", "Adscrito", "Vicario", "otro"
  ];
  stateArray = [
    "En curso", "Aplazado", "Terminado"
  ];
  get hourExist(): boolean {
    if (this.myForm)
      return true;
  }
  getPastoral() {
    this.pastoralService.getPastoral(this.id).subscribe(
      resp => {
        if (resp !== false) {
          this.delete = true;
          this.inforAcademic = resp;
          this.update = true;
          const filtered = this.inforAcademic.filter((element) => {
            const pastoralFormGroup = this.formBuilder.group(
              {
                idDataPastoral: [element.idDataPastoral],

                pastoralService: [element.pastoralService, [Validators.required]],
               
                assignedPlace: [element.assignedPlace, [Validators.required]],
               
                serviceStartDate: [element.serviceStartDate, [Validators.required]],
               
                serviceEndDate: [element.serviceEndDate],
               
                canonicalDecreeDate: [element.canonicalDecreeDate],
               
                appointmentDate: [element.appointmentDate],
                
                decreeNumber: [element.decreeNumber,[Validators.maxLength(10), Validators.pattern(this.nameService.numberPattern)]]
              });
            this.pastoral.push(pastoralFormGroup);
          });
        } else {
          this.delete = false;
          this.update = false;
        }
      }
    )
  }
 
  removeContact() {
    var size = this.pastoral.value.length;
    this.pastoral.removeAt(size - 1);
    this.create = false;
    this.update = false;
    (<FormArray>this.myForm.get('pastoralArray')).controls.forEach((group: FormGroup) => {
      (<any>Object).values(group.controls).forEach((control: FormControl) => {
        this.delete = true;
        this.update = true;
      })
    });
  }
  getGroupControl(index, fieldName) {
    return (<FormArray>this.myForm.get('pastoralArray')).at(index).get(fieldName).touched &&
      (<FormArray>this.myForm.get('pastoralArray')).at(index).get(fieldName).invalid;
  }
  get pastoral() {
    return this.myForm.get('pastoralArray') as FormArray;
  }
  get ultimate(): number {
    return this.pastoral.value;
  }
  async addPastoral() {
    if (this.create === true) {
      this.errorHandlingService.negativeAnswer('Acción incorrecta, registre el ultimo dato pastoral ingresado')
    } else {

        this.delete = false;
        this.create = true;
        this.add=true;
        const pastoralFormGroup = this.formBuilder.group(
          {
            idDataPastoral: [],

            pastoralService: [, [Validators.required]],
           
            assignedPlace: [, [Validators.required, Validators.maxLength(100)]],
           
            serviceStartDate: [, [Validators.required]],
           
            serviceEndDate: [],
           
            canonicalDecreeDate: [],
           
            appointmentDate: [],
            
            decreeNumber: [,[Validators.maxLength(10), Validators.pattern(this.nameService.numberPattern)]]

          });
        this.pastoral.push(pastoralFormGroup);
      
    }
  }
  show(level: string) {
    if (level === 'Diplomado' || level === 'Curso') {
      return true;
    }
    return false;
  }
  verifateUltimate(index: any) {
    var size = this.pastoral.value.length;
    return verifate(index, this.permissCrud.create, this.permissCrud.update, size,this.add);
  }
  getUpdatedValue(form: any) {
    var changedValues = {};
    Object.keys(form.controls)
      .forEach(key => {
        let currentControl = form.controls[key];
        if (currentControl.dirty) {
          if (currentControl.controls) {
            changedValues[key] = this.getUpdatedValue(currentControl);
          } else {
            changedValues[0] = form.value.idDataPastoral;
            changedValues[key] = currentControl.value;
          }
        }
      });
    return changedValues;
  }
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  async deletePastoral(idPastoral: any) {
    var petition = await this.errorHandlingService.question('¿Eliminar registro pastoral?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {
      this.pastoralService.deletePastoral(this.id, this.pastoral.value[idPastoral].idDataPastoral).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Registro pastoral eliminado')
            this.create = false;
            this.update = false;
            (this.myForm.controls['pastoralArray'] as FormArray).clear();
            this.getPastoral();
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      )
    }
  }
  async pastoralInformation() {
    
    (<FormArray>this.myForm.get('pastoralArray')).controls.forEach((group: FormGroup) => {
      (<any>Object).values(group.controls).forEach((control: FormControl) => {
        control.markAsTouched();
        if (control.invalid === true) {
          this.invalid = true;
        }
      })
    });
    if (this.invalid === false) {
      if (this.create === true) {
        var size = this.pastoral.value.length;
        this.pastoralService.postPastoral(
          this.pastoral.value[size - 1].pastoralService,
          this.pastoral.value[size - 1].assignedPlace,
          this.pastoral.value[size - 1].appointmentDate,
          this.pastoral.value[size - 1].decreeNumber,
          this.pastoral.value[size - 1].canonicalDecreeDate,
          this.pastoral.value[size - 1].serviceStartDate,
          this.pastoral.value[size - 1].serviceEndDate, Number(this.id)
        ).subscribe(
          resp => {
            if (resp === true) {
              this.errorHandlingService.positiveAnswer('Información pastoral creada y actualizada')
              this.add=false;
              this.delete = true;
              this.create = false;
              (this.myForm.controls['pastoralArray'] as FormArray).clear();
              this.getPastoral();
            } else {
              this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
            }
          }
        );
      } else {
        var objects = this.getUpdatedValue(this.myForm);
        if ((Object.keys(objects).length === 0) === false) {
          const parseOne = Object.entries(objects).map((e) => ({ [e[0]]: e[1] }));
          var parseTwo = parseOne[0]
          var parseThree = Object.entries(parseTwo).map((e) => ({ [e[0]]: e[1] }));
          var parseFour = parseThree[0];
          var arrayObject = Object.values(Object.entries(parseFour.pastoralArray).map((e) => ({ [e[0]]: e[1] })));
          for (var i = 0; i < arrayObject.length; i++) {
            this.pastoralService.putPastoral(arrayObject[i], Number(this.id))
              .subscribe(
                resp => {
                  if (resp === true) {
                    this.errorHandlingService.positiveAnswer('Información pastoral actualizada')
                    this.myForm.reset(this.myForm.value);
                  } else {
                    this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
                  }
                }
              )
          }
        }
      }
    } else {
      this.invalid = false;
      this.errorHandlingService.negativeAnswer('Registre los datos solicitados')
    }
  }



  errorMsgForm(nameFormValue: string, name: string, index: number): string {


    const errors = (<FormArray>this.myForm.get('pastoralArray')).at(index).get(nameFormValue).errors


    if (errors) {

      return ` ${name} ${this.errorHandlingService.name(errors)}`;
    }
  }

}
