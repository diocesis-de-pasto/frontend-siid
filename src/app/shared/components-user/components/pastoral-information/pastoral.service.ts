
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { GlobalService } from 'src/app/service/global.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PastoralService {

  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient, private globalService: GlobalService) { }

  getPastoral(id:string): Observable<any> {
    const url = `${this.baseUrl}/data-pastoral/${id}`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body;
        }
      }),
      catchError(err => of(false))
    );
  }


  postPastoral(
    pastoralService:string,
    assignedPlace: string,
    appointmentDate: Date,
    decreeNumber: string,
    canonicalDecreeDate: Date,
    serviceStartDate: Date,
    serviceEndDate: Date,
    idUser:number
  ): Observable<any> {
    const url = `${this.baseUrl}/data-pastoral`;
    const body = {
      pastoralService,
      assignedPlace,
      appointmentDate,
      decreeNumber,
      canonicalDecreeDate,
      serviceStartDate,
      serviceEndDate,
      user: {
        idUser: idUser
      }
    };
    return this.http.post<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
      catchError(err => of(false))
      );
  }
  deletePastoral(id:string,positionPastoral:number): Observable<any> {
    const url = `${this.baseUrl}/data-pastoral/${positionPastoral}`;
   const options = {
     body: {
       user: {
         idUser: id
       }
     },
   };
   return this.http.delete<any>(url,options).pipe(
     map(resp => {
       if (resp.status===200) {
         return true;
       } else {
         return false;
       }
     }),
     catchError(err => of(false))
   );
 }
  putPastoral(elements: object,id:number): Observable<any> {
    var object;
    let claves = Object.keys(elements);
    for (let i = 0; i < claves.length; i++) {
      let clave = claves[i];
      object = elements[clave];
    }
    var idDataPastoral = object[0]
    delete object[0]
    const url = `${this.baseUrl}/data-pastoral/${idDataPastoral}`;
    object.user = {
      idUser:id
    }
    return this.http.put<any>(url, object)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
         catchError(err => of(false))
      );
  }



}
