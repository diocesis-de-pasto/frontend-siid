import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient) { }


  getPersonal(idUser:string): Observable<any> {
    const url = `${this.baseUrl}/dataPerson/${idUser}`;

    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
      
        
        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
        
     
      }),
      catchError(err => of(false))
    );
  }

  postPersonal(
    expeditionDocumentDate:  any,
    expeditionDocumentPlace: any,
    firstName: string,
    secondName: any,
    firstSurname: string,
    secondSurname: any,
    cityOfBirth: any,
    departmentOfBirth: any,
    countryOfBirth: any,
    gender:any,
    photo: any,
    dateOfBirth: Date,
    residenceAddress: any,
    bloodType: string,
    contactNumberOne: string,
    contactNumberTwo: any,
    idUser:number
  ): Observable<any> {
    var user = {
      idUser: idUser
    };
    const url = `${this.baseUrl}/dataPerson`;
    const body = {
      expeditionDocumentDate,
      expeditionDocumentPlace,
      firstName,
      secondName,
      firstSurname,
      secondSurname,
      dateOfBirth,
      cityOfBirth,
      departmentOfBirth,
      countryOfBirth,
      gender,
      photo,
      residenceAddress,
      bloodType,
      contactNumberOne,
      contactNumberTwo,
      user
    };
    return this.http.post<any>(url, body)
      .pipe(
        map(resp => {


          if(resp.status===200){       
            return true;
          }else{
            return false;
          }
       
        })
        ,
        catchError(err => of(false))
      );
  }
  deletePersonal(id:string): Observable<any> {
    const url = `${this.baseUrl}/dataPerson/${id}`;
    return this.http.delete<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if (resp.status===200) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => of(false))
    );
  }
  putPersonal(body: object,id:number): Observable<any> {
    const url = `${this.baseUrl}/dataPerson`;
    var user = {
      user: {
        idUser: id
      }
    };
    var petition = Object.assign(body, user);
    return this.http.put<any>(url, petition,{observe:'response'})
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(err => of(false))
      );
  }

  deletePersonalImage(id:string): Observable<any> {
    const url = `${this.baseUrl}/dataPerson/deletePhoto/${id}`;
    return this.http.delete<any>(url).pipe(
      map(resp => {
        if (resp.status===200) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => of(false))
    );
  }
  getPersonalImage(idUser:string): Observable<any> {
    const url = `${this.baseUrl}/dataPerson/getImage/${idUser}`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
      
        
        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
        
     
      }),
     catchError(err => of(false))
    );
  }
  postPersonalImage(idUser:number,body:object ): Observable<any> {
    const url = `${this.baseUrl}/dataPerson/uploadPhoto/${idUser}`;
    return this.http.post<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            
            return true;
          } else {
            return false;
          }
        })
        ,
        catchError(err => of(false))
      );
  }
  putPersonalImage(idUser:number,body:object ): Observable<any> {
    const url = `${this.baseUrl}/dataPerson/uploadPhoto/${idUser}`;
    return this.http.post<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        })
        ,
        catchError(err => of(false))
      );
  }

}
