import { ChangeDetectorRef, Component, Input, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Person } from '../../interface/data';

import { getUpdatedValue, verifateUnit } from '../functions'
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { GlobalService } from 'src/app/service/global.service';
import { NameService } from '../../../../service/name.service';
import { Paises } from './demasPaises';
import { Colombia } from './countryColombia';
import { MatInput } from '@angular/material/input';
import { PersonalService } from './personal.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['../../../../css/formulario.css'],
  encapsulation: ViewEncapsulation.None,
})
export class PersonalInformationComponent implements OnInit {

  pais:string="";
  departamento:string="";
  city:string="";
  
  Paises= Paises.COUNTRY_WORLD;
  departamentoArray;


 cityArray;

  myForm: FormGroup = new FormGroup({});
  myFormImg: FormGroup = new FormGroup({});
  personalUser: Person;
  delete: boolean = false;
  update: boolean = false;
  create: boolean = false;
  id: string;
  urlImg = "/assets/img/userImg.png";
  imgBool = false;
  imgBoolUpdate = false
  file;
  fileSize
  showImageBoolean = false;


  @ViewChild(MatInput) input: MatInput;
  @Input() idchild: string;
  @Input() view: boolean = false;
  @Input() permissCrud: CrudPermiss;


  bloodType = [
    "O-", "O+", "B-", "B+", "A-", "A+", "AB-", "AB+"
  ];

  genderList = [
    "M", "F"
  ];
  constructor(private fb: FormBuilder,
    private errorHandlingService: ErrorHandlingService,
    private globalService: GlobalService,
    private personalService:PersonalService,
    private nameService: NameService
    ,private changeDetector: ChangeDetectorRef
  ) {
  }
  ngOnInit() {
    if (this.idchild) {
      this.id = this.idchild.toString();
    } else {
      this.id = this.globalService.userId.toString();
    }
    this.getPersonal();
    this.getPersonalImg();
    this.myForm = this.fb.group({
      expeditionDocumentDate: [],
      expeditionDocumentPlace: [],
      firstName: [, [Validators.required, Validators.maxLength(100)]],
      secondName: [, [ Validators.maxLength(100)]],
      firstSurname: [, [Validators.required]],
      secondSurname: [, [ Validators.maxLength(100)]],
      cityOfBirth: [, [ Validators.maxLength(100)]],
      departmentOfBirth: [, [ Validators.maxLength(100)]],
      dateOfBirth: [, [Validators.required]],
      countryOfBirth: [ , [ Validators.maxLength(100)]],
      gender: [],
      photo: [],
      residenceAddress: [, [ Validators.maxLength(100)]],
      bloodType: [, [Validators.required]],
      contactNumberOne: [, [Validators.required, Validators.pattern(this.nameService.numberPattern), Validators.maxLength(14)]],
      contactNumberTwo: [, [Validators.maxLength(14), Validators.pattern(this.nameService.numberPattern)]],
    });
    this.myFormImg = this.fb.group({
      photo: []
    });

  }
  select(): void {
    this.changeDetector.detectChanges();
    this.input.focus();
  } 
  getPersonal() {
    this.personalService.getPersonal(this.id).subscribe(
      resp => {
        
        if (resp !== false) {
          this.delete = true;
          this.update = true;
          this.create = false;
      

          
          
          this.myForm.controls['expeditionDocumentDate'].setValue(resp.expeditionDocumentDate);
          this.myForm.controls['expeditionDocumentPlace'].setValue(resp.expeditionDocumentPlace);
          this.myForm.controls['firstName'].setValue(resp.firstName);
          this.myForm.controls['secondName'].setValue(resp.secondName || '');
          this.myForm.controls['firstSurname'].setValue(resp.firstSurname);
          this.myForm.controls['secondSurname'].setValue(resp.secondSurname || '');
          this.myForm.controls['cityOfBirth'].setValue(resp.cityOfBirth || '');
          this.myForm.controls['departmentOfBirth'].setValue(resp.departmentOfBirth || '');
          this.myForm.controls['dateOfBirth'].setValue(resp.dateOfBirth || '');
          this.myForm.controls['residenceAddress'].setValue(resp.residenceAddress || '');
          this.myForm.controls['bloodType'].setValue(resp.bloodType);
          this.myForm.controls['countryOfBirth'].setValue(resp.countryOfBirth);
          this.myForm.controls['gender'].setValue(resp.gender);
          this.myForm.controls['photo'].setValue(resp.photo || '');
          this.myForm.controls['contactNumberOne'].setValue(resp.contactNumberOne);
          this.myForm.controls['contactNumberTwo'].setValue(resp.contactNumberTwo || '');
          this.departamento=resp.departmentOfBirth;
          this.city=resp.cityOfBirth;
          this.changeCountry(resp.countryOfBirth);
          this.cambiosDepertamentos(resp.departmentOfBirth);


 
          
        } else {
  
          
          this.delete = false;
          this.update = false;
          this.create = true;
        }
      }
    )
  }
  getPersonalImg() {
    this.imgBoolUpdate = false;
    this.personalService.getPersonalImage(this.id).subscribe(
      resp => {

        if (resp === false) {

          this.imgBool = false;
        } else {

          if (resp.image !== '') {
            this.urlImg = resp.image
            this.imgBool = true;
          } else {
            this.urlImg = "/assets/img/userImg.png";


          }
        }
      }
    )
  }
  verificatePersonal() {
    return verifateUnit(this.permissCrud.create, this.permissCrud.update, this.create);
  }
  async deletePersonal() {
    var petition = await this.errorHandlingService.question('¿Eliminar datos personales?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {
      this.personalService.deletePersonal(this.id).subscribe(
        resp => {
          if (resp === true) {
            this.myForm.reset(this.myForm.reset);
            this.delete = false;
            this.update = false;
            this.create = true;
            this.errorHandlingService.positiveAnswer('Informacion personal eliminada')
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      )
    }
  }
  async deletePersonalImage() {
    var petition = await this.errorHandlingService.question('¿Eliminar imagen?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {
      this.personalService.deletePersonalImage(this.id).subscribe(
        resp => {
          if (resp === true) {
            this.imgBool = false;
            this.urlImg = "/assets/img/userImg.png";
            this.errorHandlingService.positiveAnswer('Imagen eliminada')
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      )
    }
  }
  changeCountry(pais:string){
    if(pais==='Colombia'){

      this.departamentoArray=Colombia.COUNTRY_COLOMBIA;
      this.pais='Colombia';
    }else{

      this.departamentoArray=[];
      this.cityArray=[];
      this.departamento="";
      this.city="";
      
    }

  }
  cambiosDepertamentos(department:string){

  


    
    this.departamentoArray.filter(resp=>{

      if(resp.departamento===department){
        this.cityArray=resp.ciudades;

      }
    })
  }

  async updatePersonal() {

    if (this.myForm.invalid === true) {
      this.myForm.markAllAsTouched();
      this.errorHandlingService.negativeAnswer('Revise los campos ingresados')
    } else {

      var change: any = getUpdatedValue(this.myForm);
      change.photo = this.myForm.controls['photo'].value;

      this.personalService.putPersonal(change, Number(this.id))
        .subscribe(
          resp => {
            if (resp === true) {
              this.errorHandlingService.positiveAnswer('Información personal actualizada')
              this.myForm.reset(this.myForm.value);
            } else {
              this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
            }
          }
        )
    }

   
  }
  onFileSelectPrueba(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.myFormImg.get('photo').setValue(file);
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data ur
      reader.onload = (event: any) => { // called once readAsDataURL is completed
        const valueNoob = event.target.result;
      }
      this.urlImg
    }
  }
  async personalInformation() {


    if (this.myForm.invalid === true) {
      this.myForm.markAllAsTouched();
      this.errorHandlingService.negativeAnswer('Revise los campos ingresados')
    } else {
 

       this.personalService.postPersonal(
        this.myForm.value.expeditionDocumentDate,
        this.myForm.value.expeditionDocumentPlace,
        this.myForm.value.firstName,
        this.myForm.value.secondName,
        this.myForm.value.firstSurname,
        this.myForm.value.secondSurname,
        this.myForm.value.cityOfBirth,
        this.myForm.value.departmentOfBirth,
        this.myForm.value.countryOfBirth,
        this.myForm.value.gender,
        this.myForm.value.photo,
        this.myForm.value.dateOfBirth,
        this.myForm.value.residenceAddress,
        this.myForm.value.bloodType,
        this.myForm.value.contactNumberOne,
        this.myForm.value.contactNumberTwo,
        Number(this.id)
      ).subscribe(
        resp => {
          if (resp === true) {
            this.myForm.reset(this.myForm.value);
            this.errorHandlingService.positiveAnswer('Información personal creada')
            this.getPersonal();
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      )
    }
  }








  async onSelectFile2(event) {
    if (event.target.files && event.target.files[0]) {
      let file = event.target.files[0];
      this.file = file;

      if (file.type === ("image/png") || ("image/jpeg") || ("image/PNG") || ("image/JPEG") || ("image/JPG")) {
        var reader = new FileReader();
        let fileSize = 0;
        this.fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
        if ((Math.round(file.size * 100 / (1024 * 1024)) / 100) <= 4) {
          this.imgBoolUpdate = true;
          reader.readAsDataURL(event.target.files[0]); // read file as data ur
          reader.onload = (event: any) => { // called once readAsDataURL is completed
            const valueNoob = event.target.result;
            this.urlImg = event.target.result
            this.myFormImg.controls.photo.setValue(file);
          }
        } else {
          this.errorHandlingService.negativeAnswer('El tamaño maximo de la imagen es 4 mb')
        }
      } else {
        this.errorHandlingService.negativeAnswerPermanent('Formato de imagen no valido')
      }
    }
  }
  onSubmitPrueba() {
    const formData = new FormData();
    formData.append('file', this.myFormImg.get('photo').value);
    this.personalService.postPersonalImage(
      Number(this.id), formData
    ).subscribe(
      resp => {
        if (resp === true) {
          this.imgBool = true;
          this.imgBoolUpdate = false;
          this.errorHandlingService.positiveAnswer('Imagen registrada')
        }
      }
    )
  }

  //errors
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  errorMsgForm(nameFormValue: string, name: string): string {
    const errors = this.myForm.get(nameFormValue)?.errors;
    if (errors) {
      return ` ${name} ${this.errorHandlingService.name(errors)}`;
    }
  }
}