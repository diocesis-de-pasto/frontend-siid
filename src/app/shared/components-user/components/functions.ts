export function getUpdatedValue(form: any) {
  var changedValues = {};
  Object.keys(form.controls)
    .forEach(key => {
      let currentControl = form.controls[key];
      if (currentControl.dirty) {
        if (currentControl.controls) {
          changedValues[key] = this.getUpdatedValue(currentControl);
        } else {
          changedValues[key] = currentControl.value;
        }
      }
    });
  return changedValues;
}
export function verifate(index: any, create: number, update: number,size:number,add:boolean) {
  if (update === 0 && create === 0) {
    return false;
  } else if (create === 1 && update === 0) {
    if ((index + 1) === size && add===true) {
      return true;
    } else {
      return false;
    }
  } else if (create === 0 && update === 1) {
    return true;
  } else {
    return true;
  }
}
export function verifateUnit(create: number, update: number, createBoolean: boolean) {
  if (createBoolean === false) {
    if (create === 1 && update === 0) {
      return false;
    } else if (create === 0 && update === 1) {
      return true;
    }
    else if (create === 1 && update === 1) {
      return true;
    }
    else {
      return false;
    }
  } else {
    if (create === 1 && update === 0) {
      return true;
    } else if (create === 0 && update === 1) {
      return true;
    }
    else if (create === 1 && update === 1) {
      return true;
    }
    else {
      return false;
    }
  }
}
