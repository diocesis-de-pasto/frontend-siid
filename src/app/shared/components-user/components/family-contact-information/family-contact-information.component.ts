import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { GlobalService } from 'src/app/service/global.service';
import { verifateUnit } from '../functions';
import { NameService } from '../../../../service/name.service';
import { FamilyService } from './family.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';
@Component({
  selector: 'app-family-contact-information',
  templateUrl: './family-contact-information.component.html',
  styleUrls: ['../../../../css/formulario.css'],
})
export class FamilyContactInformationComponent implements OnInit {
  public familyArray = [
    "Padre", "Madre", "Hermano/Hermana", "Hijo/Hija", "Sobrino/Sobrina", "Tio/Tia", "Abuelo/Abuela", "Otro",
  ];
  id: string;
  public myForm: FormGroup = new FormGroup({});
  public delete: boolean = false;
  public update: boolean = false;
  public create: boolean = false;
  @Input() idchild: string;
  @Input() view: boolean = false;
  @Input() permissCrud: CrudPermiss;
  constructor(private fb: FormBuilder,
    private errorHandlingService: ErrorHandlingService,
    private familyService: FamilyService,
    private globalService: GlobalService,
    private nameService: NameService
  ) {
  }
  ngOnInit() {
    if (this.idchild) {
      this.id = this.idchild.toString();
    } else {
      this.id = this.globalService.userId.toString();
    }
    this.getFamily();
    this.myForm = this.fb.group({
      idFamilyContact: [],
      fullName: [, [Validators.required, Validators.maxLength(100)]],
      phoneOne: [, [Validators.required, Validators.pattern(this.nameService.numberPattern), Validators.maxLength(14)]],
      phoneTwo: [, [Validators.maxLength(14), Validators.pattern(this.nameService.numberPattern)]],
      phoneThree: [, [Validators.maxLength(14), Validators.pattern(this.nameService.numberPattern)]],
      relationship: [, [Validators.required]],
    });
  }
  verificateFamily() {
    return verifateUnit(this.permissCrud.create, this.permissCrud.update, this.create);
  }
  getUpdatedValue(form: any) {
    var changedValues = {};
    Object.keys(form.controls)
      .forEach(key => {
        let currentControl = form.controls[key];
        if (currentControl.dirty) {
          if (currentControl.controls) {
            changedValues[key] = this.getUpdatedValue(currentControl);
          } else {
            changedValues[0] = form.value.idFamilyContact;
            changedValues[key] = currentControl.value;
          }
        }
      });
    return changedValues;
  }
  getFamily() {
    this.familyService.getfamilyContact(this.id).subscribe(
      resp => {
        if (resp.idFamilyContact) {
          this.delete = true;
          this.update = true;
          this.create = false;
          this.myForm.controls['idFamilyContact'].setValue(resp.idFamilyContact);
          this.myForm.controls['fullName'].setValue(resp.fullName);
          this.myForm.controls['phoneOne'].setValue(resp.phoneOne);
          this.myForm.controls['phoneTwo'].setValue(resp.phoneTwo);
          this.myForm.controls['phoneThree'].setValue(resp.phoneThree);
          this.myForm.controls['relationship'].setValue(resp.relationship);
        } else {
          this.delete = false;
          this.update = false;
          this.create = true;
        }
      }
    )
  }
  async familyContact() {
    if (this.myForm.invalid === true) {
      this.myForm.markAllAsTouched();
      this.errorHandlingService.negativeAnswer('Ingrese los datos requeridos')
    } else {
      this.familyService.postfamilyContact(
        this.myForm.value.fullName,
        this.myForm.value.phoneOne,
        this.myForm.value.phoneTwo,
        this.myForm.value.phoneThree,
        this.myForm.value.relationship,
        Number(this.id)
      ).subscribe(
        resp => {
          if (resp === true) {
            this.myForm.reset(this.myForm.value);
            this.errorHandlingService.positiveAnswer('Información familiar actualizada')
            this.getFamily();
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      );

    }
  }
  async updatePersonal() {
    var change = this.getUpdatedValue(this.myForm);
    this.familyService.putfamilyContact(change, Number(this.id))
      .subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Contacto familiar actualizado')
            this.myForm.reset(this.myForm.value);
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      );
  }
  async deleteFamily() {
    var petition = await this.errorHandlingService.question('¿Eliminar datos contacto familiar?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {
      this.familyService.deleteFamily(this.id).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Registro familiar eliminado');
            this.myForm.reset(this.myForm.reset);
            this.delete = false;
            this.update = false;
            this.create = true;
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      )
    }
  }
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  errorMsgForm(nameFormValue: string, name: string): string {
    const errors = this.myForm.get(nameFormValue)?.errors;
    if (errors) {
 
      return ` ${name} ${this.errorHandlingService.name(errors)}`;
    }
  }
}
