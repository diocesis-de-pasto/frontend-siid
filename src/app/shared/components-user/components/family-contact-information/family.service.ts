
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { GlobalService } from 'src/app/service/global.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FamilyService {

  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient, private globalService: GlobalService) { }

  postfamilyContact(
    fullName: string,
    phoneOne: string,
    phoneTwo: string,
    phoneThree: string,
    relationship: string,
    id:number
  ): Observable<any> {
    var user = {
      idUser: id
    };
    const url = `${this.baseUrl}/family-contact`;
    const body = {
      fullName,
      phoneOne,
      phoneTwo,
      phoneThree,
      relationship,
      user
    };
    return this.http.post<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        })
        ,
        catchError(err => of(false))
      );
  }
  putfamilyContact(body: object,id:number): Observable<any> {
    var idfamilyContact= body[0];
    const url = `${this.baseUrl}/family-contact/${idfamilyContact}`;
    var user = {
      user: {
        idUser: id
      }
    };
    var petition = Object.assign(body, user);
    return this.http.put<any>(url, petition)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
      catchError(err => of(false))
      );
  }
  getfamilyContact(id:string): Observable<any> {
    const url = `${this.baseUrl}/family-contact/${id}`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body;
        }
      }),
      catchError(err => of(false))
    );
  }

  deleteFamily(id:string): Observable<any> {
    const url = `${this.baseUrl}/family-contact/`;
    const options = {
      body: {
        user: {
          idUser: id
        }
      },
    };
    return this.http.delete<any>(url,options).pipe(
      map(resp => {
        if (resp.status===200) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => of(false))
    );
  }

}
