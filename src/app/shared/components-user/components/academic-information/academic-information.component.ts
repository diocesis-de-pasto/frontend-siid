import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Academic } from '../../interface/data';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { GlobalService } from 'src/app/service/global.service';
import { verifate } from '../functions'
import { NameService } from 'src/app/service/name.service';
import { AcademicService } from './academic.service';






import { Years } from './year';
import { Paises } from '../personal-information/demasPaises';
import { Colombia } from '../personal-information/countryColombia';
import { CrudPermiss } from 'src/app/interface/crud-permiss';


@Component({
  selector: 'app-academic-information',
  templateUrl: './academic-information.component.html',
  styleUrls: ['../../../../css/formulario.css'],
 
})
export class AcademicInformationComponent implements OnInit {


  public years= Years.YEARS;
  public pais:string="";
  public departamento:string="";
  public city:string="";
  public cityArray;
  public Paises= Paises.COUNTRY_WORLD;
  public departamentoArray= Colombia.COUNTRY_COLOMBIA;
  public selectedItem: string;
  public inforAcademic: Academic[];
  public invalid: boolean = false;
  public id: string;
  public delete: boolean = false;
  public create: boolean = false;
  public update: boolean = false;
  public add: boolean = false;
  public myForm: FormGroup = new FormGroup({});
  public chosenYearDate: Date;
  @Input() idchild: string;
  @Input() permissCrud: CrudPermiss;
  @Input() disabled = true;
  @Input() view: boolean = false;


  constructor(private fb: FormBuilder,
    private academicaService:AcademicService,
    private errorHandlingService: ErrorHandlingService,
    private formBuilder: FormBuilder
    , private nameService: NameService,
    private globalService: GlobalService
  ) {
  }
  academicType = [
    "Primarios", "Secundarios", "Eclesiasticos", "Universitario", "Pregado", "Postgrado", "Diplomado", "Curso",'Técnico','Tecnológico','Seminario'
  ];
  stateArray = [
    "En curso", "Aplazado", "Terminado"
  ];
  get hourExist(): boolean {
    if (this.myForm)
      return true;
  }
   dateToday(estado:any) {

    if (estado === 'En curso') {
      return new Date();
    }
    return false;


  }
  dateTodayTitle(estado:any) {

    if (estado === 'En curso' || estado === 'Terminado') {

      var yearActually = new Date();

      return yearActually.getFullYear();
    }
    return '2040';
  
  }




  ngOnInit() {


    
    
    if (this.idchild) {
      this.id = this.idchild.toString();
    } else {
      this.id = this.globalService.userId.toString();
    }
    
    this.getAcademic();
    this.myForm = this.fb.group({
      input: new FormControl({ value: '', disabled: this.disabled }),
      institutionName: this.formBuilder.array([]),
    });
  }
  verifateUltimate(index: any) {
    var size = this.institutionName.value.length;
    return verifate(index, this.permissCrud.create, this.permissCrud.update, size, this.add);
  }
  removeContact() {
    var size = this.institutionName.value.length;
    this.institutionName.removeAt(size - 1);
    this.create = false;
    this.update = false;
    var ran = (<FormArray>this.myForm.get('institutionName')).controls.forEach((group: FormGroup) => {
      (<any>Object).values(group.controls).forEach((control: FormControl) => {
        
        this.delete = true;
        this.update = true;
      })
    });
  }
  getAcademic() {
    this.academicaService.getAcademic(this.id).subscribe(
      resp => {
        if (resp !== false) {
          this.delete = true;
          this.update = true;
          this.inforAcademic = resp;
          const filtered = this.inforAcademic.filter((element) => {
            const institutionNameFormGroup = this.formBuilder.group(
              {
                levelOfStudy: [element.levelOfStudy, [Validators.required]],
                institutionName: [element.institutionName, [Validators.required, Validators.maxLength(100)]],
               
                titleReceived: [element.titleReceived,[Validators.required, Validators.maxLength(100)]],
                lastYearStudied: [element.lastYearStudied, [Validators.required,Validators.maxLength(4)]],
                year: [element.year],
                culminationDate: [element.culminationDate],
                projectedGradeDate: [element.projectedGradeDate],
                state: [element.state, [Validators.required]],
                city: [element.city, [,Validators.maxLength(100)]],
                country: [element.country, [,Validators.maxLength(100)]],
                department: [element.department, [,Validators.maxLength(100)]],
                numberOfHours: [element.numberOfHours, [Validators.required]],
                idDataAcademy: [element.idDataAcademy],


              });

              
          
    
            this.institutionName.push(institutionNameFormGroup);
          });
        } else {
          this.delete = false;
          this.update = false;
        }
      }
    )
  }
  async deleteAcademic(idAcademic: any) {
    var petition = await this.errorHandlingService.question('¿Eliminar registro academico?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {
      this.academicaService.deleteAcademic(this.id, this.institutionName.value[idAcademic].idDataAcademy).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Registro academico eliminado')
            this.create = false;
            this.update = false;
            (this.myForm.controls['institutionName'] as FormArray).clear();
            this.getAcademic();
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      )
    }
  }
  get institutionName() {
    return this.myForm.get('institutionName') as FormArray;
  }
  get ultimate(): number {
    return this.institutionName.value;
  }
  async addAcademic() {
    
    if (this.create === true) {
      this.errorHandlingService.negativeAnswer('Acción incorrecta, registre el ultimo dato académico ingresado')
    } else {


      this.add = true;
      this.delete = false;
      this.create = true;
      const institutionNameFormGroup = this.formBuilder.group(
        {
          levelOfStudy: [, [Validators.required]],
          institutionName: [, [Validators.required, Validators.maxLength(100)]],
         
          titleReceived: [,[Validators.required,  Validators.maxLength(100)]],
          lastYearStudied: [, [Validators.required,Validators.maxLength(4), Validators.pattern(this.nameService.numberPattern)]],
          year: [],

          culminationDate: [],
          projectedGradeDate: [],
         
          state: [, [Validators.required]],

          city: [, [,Validators.maxLength(100)]],
          country: [, [,Validators.maxLength(100)]],
          department: [, [,Validators.maxLength(100)]],
          numberOfHours: [0, [Validators.required]],
          idDataAcademy: [],
        });

        
      this.institutionName.push(institutionNameFormGroup);

    }
  }
  changeValidators(estado:string,index:number){
    if (estado==='Aplazado' ||estado==='En curso' ) {
      (<FormArray>this.myForm.get('institutionName')).at(index).get('year').setValidators([]);
      (<FormArray>this.myForm.get('institutionName')).at(index).get('year').updateValueAndValidity();
    }else{
      (<FormArray>this.myForm.get('institutionName')).at(index).get('year').setValidators([Validators.required]);
      (<FormArray>this.myForm.get('institutionName')).at(index).get('year').updateValueAndValidity();
    }


    
  }
  dale(departamento:string){
 

                this.cambiosDepertamentos(departamento);
    
       
  }



  show(level: string) {
    if (level === 'Diplomado' || level === 'Curso') {
      return true;
    }
    return false;
  }
  getGroupControl(index, fieldName) {

    return (<FormArray>this.myForm.get('institutionName')).at(index).get(fieldName).touched &&
      (<FormArray>this.myForm.get('institutionName')).at(index).get(fieldName).invalid;
  }
  getUpdatedValue(form: any) {
    var changedValues = {};
    Object.keys(form.controls)
      .forEach(key => {
        let currentControl = form.controls[key];
        if (currentControl.dirty) {
          if (currentControl.controls) {
            changedValues[key] = this.getUpdatedValue(currentControl);
          } else {
            changedValues[0] = form.value.idDataAcademy;
            changedValues[key] = currentControl.value;
          }
        }
      });
    return changedValues;
  }
  async academicInformatio() {


    
    (<FormArray>this.myForm.get('institutionName')).controls.forEach((group: FormGroup) => {

      
      (<any>Object).values(group.controls).forEach((control: FormControl) => {

        
        control.markAsTouched();
        if (control.invalid === true) {
          this.invalid = true;
        }
      })
    });
    if (this.invalid === false) {
      if (this.create === true) {
        var size = this.institutionName.value.length;
        if (this.institutionName.value[size - 1].numberOfHours === null) {
          this.institutionName.value[size - 1].numberOfHours = 0;
        }
        this.academicaService.postAcademic(
          this.institutionName.value[size - 1].levelOfStudy,
          this.institutionName.value[size - 1].institutionName,
          this.institutionName.value[size - 1].titleReceived,
          this.institutionName.value[size - 1].year,
          this.institutionName.value[size - 1].lastYearStudied,
          this.institutionName.value[size - 1].culminationDate,
          this.institutionName.value[size - 1].projectedGradeDate,
          this.institutionName.value[size - 1].state,
          this.institutionName.value[size - 1].numberOfHours,
          this.institutionName.value[size - 1].city,
          this.institutionName.value[size - 1].country,
          this.institutionName.value[size - 1].department,
          
          Number(this.id)
        ).subscribe(
          resp => {
       
            
            if (resp === true) {
              
              
              this.add = false;
              this.errorHandlingService.positiveAnswer('Información academica creada y actualizada')
              this.myForm.reset(this.myForm.value);
              this.create = false;
              (this.myForm.controls['institutionName'] as FormArray).clear();
              this.getAcademic();
            } else {
              this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
            }
          }
        );
      } else {
        var objects = this.getUpdatedValue(this.myForm);
        if ((Object.keys(objects).length === 0) === false) {
          const parseOne = Object.entries(objects).map((e) => ({ [e[0]]: e[1] }));
          var parseTwo = parseOne[0]
          var parseThree = Object.entries(parseTwo).map((e) => ({ [e[0]]: e[1] }));
          var parseFour = parseThree[0];
          var arrayObject = Object.values(Object.entries(parseFour.institutionName).map((e) => ({ [e[0]]: e[1] })));
          for (var i = 0; i < arrayObject.length; i++) {
            this.academicaService.putAcademic(arrayObject[i], this.id)
              .subscribe(
                resp => {
                  if (resp === true) {
                    this.errorHandlingService.positiveAnswer('Información academica actualizada');
                    this.myForm.reset(this.myForm.value);
                  } else {
                    this.errorHandlingService.negativeAnswer('El servicio no esta disponible2');
                  }
                }
              )
          }
        }
      }
    } else {
      this.invalid = false;
      this.errorHandlingService.negativeAnswer('Registre los datos solicitados')
    }
  }


  changeCountry(control:any){

  control.controls.department.setValue();
  control.controls.city.setValue();

    

  

  }
  cambiosDepertamentos(department:string){

    this.cityArray=[];
    this.departamentoArray.filter(resp=>{

      if(resp.departamento===department){
        this.cityArray=resp.ciudades;

      }
    })
  }

  cityArrayMetodo(department:string):string[]{


    for (let index = 0; index <  this.departamentoArray.length; index++) {
      if(this.departamentoArray[index].departamento===department){
        return this.departamentoArray[index].ciudades;

     }
      
    }
    return [];
  }


  errorMsgForm(nameFormValue: string, name: string, index: number): string {


    const errors = (<FormArray>this.myForm.get('institutionName')).at(index).get(nameFormValue).errors 


    if (errors) {
   
      return ` ${name} ${this.errorHandlingService.name(errors)}`;
    }
  }
}