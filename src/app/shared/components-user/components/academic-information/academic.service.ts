
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { GlobalService } from 'src/app/service/global.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AcademicService {

  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient, private globalService: GlobalService) { }


  postAcademic(
    levelOfStudy: string,
    institutionName: string,
    titleReceived: string,
    year: Date,
    lastYearStudied:string,
    culminationDate: Date,
    projectedGradeDate: Date,
    state: string,
    numberOfHours: number,
    city: string,
    country: string,
    department: string,
    idUser:number,
  ): Observable<any> {
    const url = `${this.baseUrl}/data-academy`;
    const body = {
      levelOfStudy,
      institutionName,
      titleReceived,
      year,
      lastYearStudied,
      culminationDate,
      projectedGradeDate,
      state,
      numberOfHours,
      city,
      country,
      department,
      user: {
        idUser:idUser
      }
    };
    return this.http.post<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }), catchError(err => of(false))
      );
  }
  putAcademic(elements: object,id:string): Observable<any> {
    var object;
    let claves = Object.keys(elements);
    for (let i = 0; i < claves.length; i++) {
      let clave = claves[i];
      object = elements[clave];
    }
    var idInformationAcademic = object[0]
    delete object[0]
    const url = `${this.baseUrl}/data-academy/${idInformationAcademic}`;
    object.user = {
      idUser: id
    }
    return this.http.put<any>(url, object)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(err => of(false))
      );
  }
  deleteAcademic(id:string,positionAcademic:number): Observable<any> {
     const url = `${this.baseUrl}/data-academy/${positionAcademic}`;
    const options = {
      body: {
        user: {
          idUser: id
        }
      },
    };
    return this.http.delete<any>(url,options).pipe(
      map(resp => {
        if (resp.status===200) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => of(false))
    );
  }
  getAcademic(id:string): Observable<any> {
    const url = `${this.baseUrl}/data-academy/${id}`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body;
        }
      }),
      catchError(err => of(false))
    );
  }


}
