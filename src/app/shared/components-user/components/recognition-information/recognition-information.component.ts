import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Recognition } from '../../interface/data';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { GlobalService } from 'src/app/service/global.service';
import { verifate } from '../functions';
import { NameService } from 'src/app/service/name.service';
import { RecognitionService } from './recognition.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';

@Component({
  selector: 'app-recognition-information',
  templateUrl: './recognition-information.component.html',
  styleUrls: ['../../../../css/formulario.css'],
})
export class RecognitionInformationComponent implements OnInit {
  selectedItem: string;
  recognition: Recognition[];
  create: boolean = false;
  invalid: boolean = false;
  delete: boolean = false;
  update: boolean = false;
  add:boolean=false;
  myForm: FormGroup = new FormGroup({});
  @Input() idchild: string;
  @Input() permissCrud: CrudPermiss;
  @Input() view: boolean =false;
  id: string;
  constructor(private fb: FormBuilder,
    private recognitionService: RecognitionService,
    private errorHandlingService: ErrorHandlingService,
    private formBuilder: FormBuilder,
    private globalService: GlobalService,
    private nameService: NameService
  ) {
  }
  academicType = [
    "Primarios", "Secundarios", "Eclesiasticos", "Universitario", "Pregado", "Postgrado", "Diplomado", "Curso"
  ];
  stateArray = [
    "En curso", "Aplazado", "Terminado"
  ];
  get hourExist(): boolean {
    if (this.myForm)
      return true;
  }
  verifateUltimate(index: any) {
    var size = this.institutionName.value.length;
      return verifate(index,this.permissCrud.create,this.permissCrud.update,size,this.add);
  }
  getRecognition() {
    this.recognitionService.getRecognition(this.id).subscribe(
      resp => {
        if (resp !== false) {
          this.delete = true;
          this.recognition = resp;
          this.update = true;
          const filtered = this.recognition.filter((element) => {
            const institutionNameFormGroup = this.formBuilder.group(
              {
                idRecognition: [element.idRecognition, [Validators.required]],
                recognitionMotive: [element.recognitionMotive, [Validators.required, Validators.maxLength(100)]],
                recognitionDate: [element.recognitionDate, [Validators.required]],
                entityThatIssuesRecognition: [element.entityThatIssuesRecognition, [Validators.required, Validators.maxLength(100)]]
              });
            this.institutionName.push(institutionNameFormGroup);
          });
        } else {
          this.delete = false;
          this.update = false;
        }
      }
    );
  }
  async deleteRecognition(idRecognition: any) {
    var petition = await this.errorHandlingService.question('¿Eliminar registro reconocimiento?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {
      this.recognitionService.deleteRecognition(this.id, this.institutionName.value[idRecognition].idRecognition,).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Registro pastoral eliminado')
            this.create = false;
            this.update = false;
            (this.myForm.controls['institutionName'] as FormArray).clear();
            this.getRecognition();
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      )
    }
  }
  removeContact() {
    var size = this.institutionName.value.length;
    this.institutionName.removeAt(size - 1);
    this.create = false;
    this.update = false;
    (<FormArray>this.myForm.get('institutionName')).controls.forEach((group: FormGroup) => {
      (<any>Object).values(group.controls).forEach((control: FormControl) => {
        this.delete = true;
        this.update = true;
      })
    });
  }
  ngOnInit() {
    if (this.idchild) {
      this.id = this.idchild.toString();
    } else {
      this.id = this.globalService.userId.toString();
    }

    
    this.getRecognition();
    this.myForm = this.fb.group({
      institutionName: this.formBuilder.array([]),
    });
  }
  get institutionName() {
    return this.myForm.get('institutionName') as FormArray;
  }
  get ultimate(): number {
    return this.institutionName.value;
  }
  async addRecognition() {
    if (this.create === true) {
      this.errorHandlingService.negativeAnswer('Acción incorrecta, registre el ultimo reconocimiento ingresado')
    } else {

        this.add=true;
        this.create = true;
        this.delete = false;
        const institutionNameFormGroup = this.formBuilder.group(
          {
            idRecognition: null,
            recognitionMotive: [, [Validators.required, Validators.maxLength(100)]],
            recognitionDate: [, Validators.required],
            entityThatIssuesRecognition: [, [Validators.required,Validators.maxLength(100)]]
          });
        this.institutionName.push(institutionNameFormGroup);
      
    }
  }
  show(level: string) {
    if (level === 'Diplomado' || level === 'Curso') {
      return true;
    }
    return false;
  }
  getUpdatedValue(form: any) {
    var changedValues = {};
    Object.keys(form.controls)
      .forEach(key => {
        let currentControl = form.controls[key];
        if (currentControl.dirty) {
          if (currentControl.controls) {
            changedValues[key] = this.getUpdatedValue(currentControl);
          } else {
            changedValues[0] = form.value.idRecognition;
            changedValues[key] = currentControl.value;
          }
        }
      });
    return changedValues;
  }
  getGroupControl(index, fieldName) {
    return (<FormArray>this.myForm.get('institutionName')).at(index).get(fieldName).touched &&
      (<FormArray>this.myForm.get('institutionName')).at(index).get(fieldName).invalid;
  }
  async recognitionInformation() {
     (<FormArray>this.myForm.get('institutionName')).controls.forEach((group: FormGroup) => {
      (<any>Object).values(group.controls).forEach((control: FormControl) => {
        control.markAsTouched();
        if (control.invalid === true) {
          this.invalid = true;
        }
      })
    });
    if (this.invalid === false) {
      if (this.create === true) {
        var size = this.institutionName.value.length;
        this.recognitionService.postRecognition(
          this.institutionName.value[size - 1].recognitionMotive,
          this.institutionName.value[size - 1].recognitionDate,
          this.institutionName.value[size - 1].entityThatIssuesRecognition,
          Number(this.id)
        ).subscribe(
          resp => {
            if (resp === true) {
              this.errorHandlingService.positiveAnswer('Reconocimiento  actualizado')
              this.add=false;
              this.delete = true;
              this.create = false;
              (this.myForm.controls['institutionName'] as FormArray).clear();
              this.getRecognition();
            } else {
              this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
            }
          }
        );
      } else {
        var objects = this.getUpdatedValue(this.myForm);
        if ((Object.keys(objects).length === 0) === false) {
          const parseOne = Object.entries(objects).map((e) => ({ [e[0]]: e[1] }));
          var parseTwo = parseOne[0]
          var parseThree = Object.entries(parseTwo).map((e) => ({ [e[0]]: e[1] }));
          var parseFour = parseThree[0];
          var arrayObject = Object.values(Object.entries(parseFour.institutionName).map((e) => ({ [e[0]]: e[1] })));
          for (var i = 0; i < arrayObject.length; i++) {
            objects = {}
            this.recognitionService.putRecognition(arrayObject[i], Number(this.id))
              .subscribe(
                resp => {
                  if (resp === true) {
                    this.errorHandlingService.positiveAnswer('Reconocimiento actualizado')
                    this.myForm.reset(this.myForm.value);
                  } else {
                    this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
                  }
                }
              )
          }
        }
      }
    } else {
      this.invalid = false;
      this.errorHandlingService.negativeAnswer('Registre los datos solicitados')
    }
  }
  errorMsgForm(nameFormValue: string, name: string, index: number): string {


    const errors = (<FormArray>this.myForm.get('institutionName')).at(index).get(nameFormValue).errors


    if (errors) {

      return ` ${name} ${this.errorHandlingService.name(errors)}`;
    }
  }
}
