
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { GlobalService } from 'src/app/service/global.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecognitionService {

  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient, private globalService: GlobalService) { }

  deleteRecognition(id:string,positionRecognition:number): Observable<any> {
    const url = `${this.baseUrl}/recognitions/${positionRecognition}`;
   const options = {
     body: {
       user: {
         idUser: id
       }
     },
   };
   return this.http.delete<any>(url,options).pipe(
     map(resp => {
       if (resp.status===200) {
         return true;
       } else {
         return false;
       }
     }),
     catchError(err => of(false))
   );
 }



 postRecognition(
   recognitionMotive: string,
   recognitionDate:Date,
   entityThatIssuesRecognition: string,
   id:number
 ): Observable<any> {
   const url = `${this.baseUrl}/recognitions`;
   const body = {
     recognitionMotive,
     recognitionDate,
     entityThatIssuesRecognition,
     user: {
       idUser: id
     }
   };
   return this.http.post<any>(url, body)
     .pipe(
       map(resp => {
           if (resp.status === 200) {
             return true;
           } else {
             return false;
           }
       }),
     catchError(err => of(false))
     );
 }
 putRecognition(elements: object,id:number): Observable<any> {
   var object;
   let claves = Object.keys(elements);
   for (let i = 0; i < claves.length; i++) {
     let clave = claves[i];
     object = elements[clave];
   }
   var idDataPastoral = object[0]
   delete object[0]
   const url = `${this.baseUrl}/recognitions/${idDataPastoral}`;
   object.user = {
     idUser: id
   }
   return this.http.put<any>(url, object)
     .pipe(
       map(resp => {
         if (resp.status === 200) {
           return true;
         } else {
           return false;
         }
       }),
       catchError(err => of(false))
     );
 }
 getRecognition(id:string): Observable<any> {
   const url = `${this.baseUrl}/recognitions/${id}`;
   return this.http.get<any>(url,{observe:'response'}).pipe(
     map(resp => {
      if(resp.status===204){       
        return false;
      }else{
        return resp.body;
      }
     }),
     catchError(err => of(false))
   );
 }



}
