import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { GlobalService } from '../../../../service/global.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient, private globalService: GlobalService) { }

  createUser(object:object){
    const url = `${this.baseUrl}/user`;   
 
    return this.http.post<any>(url,object)
    .pipe(
      map(resp => {
        if (resp.status === 204) {
          return false;
        } else {
          return true;
        }
      }),
      catchError(err=> of(false))                              
    )
  }

  putUser(body: object): Observable<any> {
    const url = `${this.baseUrl}/user`;




    return this.http.put<any>(url, body)
      .pipe(

        catchError(err => of(false))
      );
  }
  getUser(id: string): Observable<any> {
    const url = `${this.baseUrl}/user/${id}`;
    return this.http.get<any>(url, { observe: 'response' }).pipe(
      map(resp => {
        if (resp.status === 204) {
          return false;
        } else {
          return resp.body;
        }
      }),
      catchError(err => of(false))
    );
  }


  listInstitution() {
    const url = `${this.baseUrl}/institution`;
    return this.http.get<any>(url, { observe: 'response' }).pipe(
      map(resp => {
        if (resp.status === 204) {
          return false;
        } else {

          return resp.body;
        }

      }),
      catchError(err => of([]))
    );
  }
  listTypeUser() {
    const url = `${this.baseUrl}/types-user`;
    return this.http.get<any>(url, { observe: 'response' }).pipe(
      map(resp => {
        if (resp.status === 204) {
          return false;
        } else {

          return resp.body;
        }

      }),
      catchError(err => of([]))
    );
  }
  ////
  listUser() {
    const url = `${this.baseUrl}/user`;

    return this.http.get<any>(url, { observe: 'response' }).pipe(
      map(resp => {


        if (resp.status === 204) {
          return false;
        } else {
          return resp.body
        }
      }),
      catchError(err => of(false))
    );

  }
  deleteUser(id: number) {
    const url = `${this.baseUrl}/user/${id}`;
    return this.http.delete<any>(url)
  }
  getUserId(id: number): Observable<any> {

    const url = `${this.baseUrl}/user/${id}`;
    return this.http.get<any>(url, { observe: 'response' }).pipe(
      map(resp => {


        if (resp.status === 204) {
          return false;
        } else {
          return resp.body
        }
      }),
      catchError(err => of(false))
    );
  }
  getCarnetId(id: number): Observable<any> {
    const url = `${this.baseUrl}/carnet/${id}`;
    return this.http.get<any>(url, { responseType: 'blob' as 'json' }).pipe(
      catchError(err => of(false))
    );
  }
  getDataPersonId(id: number): Observable<any> {
    const url = `${this.baseUrl}/dataPerson/${id}`;
    return this.http.get<any>(url, { observe: 'response' }).pipe(
      map(resp => {


        if (resp.status === 204) {
          return false;
        } else {
          return resp.body
        }
      }),
      catchError(err => of(false))
    );
  }
  get person(){
    return true;
    // if(localStorage.getItem('rse')){
    //   let dataPerson:User = jwt_decode(localStorage.getItem('rse'));
    //   return dataPerson;
    // }else{
    //    this.userService.getUserId(this.globalService.userId).subscribe(
    //     resp=>{
    //       if(resp!==false){
    //         localStorage.setItem('rse',btoa(resp));
    //         let dataPerson:User = jwt_decode(localStorage.getItem('rse'));
    //         return dataPerson;
    //       }
    //     }
    //   );
    // }
  }
  updateState(email: string, password: string, numberOfAdmissions: number): Observable<any> {

    const url = `${this.baseUrl}/user`;
    const body = { email, password, numberOfAdmissions };
  
    return this.http.put<any>(url, body).pipe(
      map(resp => {
        if (resp.status === 200) {
          return true;
        } else {
          return false;
        }

      }),
      catchError(err => of(false))
    );;

  }
}
