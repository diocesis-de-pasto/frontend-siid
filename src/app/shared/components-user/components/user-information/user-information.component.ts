import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { GlobalService } from 'src/app/service/global.service';

import { UserService } from './user.service';
import { verifateUnit } from '../functions';
import { InjectTokenService } from '../../../../sidebar/services/inject-token.service';
import { RoleService } from 'src/app/role-diocesano/service/role.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';

@Component({
  selector: 'app-user-information',
  templateUrl: './user-information.component.html',
  styleUrls: ['../../../../css/formulario.css'],
})
export class UserInformationComponent implements OnInit {
  public stateArray = [
    'enable', 'desable'
  ];

  public typeDocument = ['C.C', 'TARJETA DE IDENTIDAD', 'PASAPORTE', 'DOCUMENTO EXTRANGERO'
  ];

  public rolArray = [];
  public typeUsersArray = [];
  public institutionArray = [];
  public id: string;
  public myForm: FormGroup = new FormGroup({});
  @Input() idchild: string;
  @Input() view: boolean = false;
  @Input() permissCrud: CrudPermiss;
  public rol: CrudPermiss;
  public institution: CrudPermiss;
  public typeUser: CrudPermiss;
  public state: any;

  constructor(private fb: FormBuilder,
    private errorHandlingService: ErrorHandlingService,
    private roleService: RoleService,
    private globalService: GlobalService,
    private injectTokenService: InjectTokenService,
    private userService: UserService
  ) {
  }
  ngOnInit() {

    this.rol = this.injectTokenService.permission('Administración de roles');
    this.institution = this.injectTokenService.permission('Administración instituciones');
    this.typeUser = this.injectTokenService.permission('Administración de tipos de usuario');



    if (this.idchild) {
      this.id = this.idchild.toString();
    } else {
      this.id = this.globalService.userId.toString();
    }
    this.myForm = this.fb.group({
      idUser: [],
      documentId: [],
      documentType: [],
      email: [],
      emailCorporate: [],
      state: [],
      uuidQR: [],
      idRole: [],
      typeUser: [],
      idInstitution: [],

    });

    this.getUser();

    if (this.rol.read === 1) {
      this.getRol();
    }
    if (this.institution.read === 1) {
      this.getInstituion();
    }
    if (this.typeUser.read === 1) {

      this.getTypeUsers();
    }


  }
  verificatePersonal() {
    return verifateUnit(this.permissCrud.create, this.permissCrud.update, true);
  }
  getUpdatedValue(form: any) {
    var changedValues = {};
    Object.keys(form.controls)
      .forEach(key => {
        let currentControl = form.controls[key];
        if (currentControl.dirty) {
          if (currentControl.controls) {
            changedValues[key] = this.getUpdatedValue(currentControl);
          } else {
            changedValues[0] = form.value.idFamilyContact;
            changedValues[key] = currentControl.value;
          }
        }
      });
    return changedValues;
  }
  getUser() {

    this.userService.getUser(this.id).subscribe(
      resp => {


        if (resp !== false) {



          this.myForm.controls['idUser'].setValue(resp.idUser);
          this.myForm.controls['documentId'].setValue(resp.documentId);
          this.myForm.controls['documentType'].setValue(resp.documentType);
          this.myForm.controls['email'].setValue(resp.email);
          this.myForm.controls['emailCorporate'].setValue(resp.emailCorporate);
          this.myForm.controls['state'].setValue(resp.state);
          this.myForm.controls['uuidQR'].setValue(resp.uuidQR);
          this.myForm.controls['idRole'].setValue(resp.idRole);
          this.myForm.controls['typeUser'].setValue(resp.idTypeUser);
          this.myForm.controls['idInstitution'].setValue(resp.idInstitution);




        }
      }
    )
  }
  getRol() {
    this.roleService.listRole().subscribe(
      resp => {

        if (resp !== false) {



          this.rolArray = resp;





        }
      }
    )
  }
  getInstituion() {
    this.userService.listInstitution().subscribe(
      resp => {
        if (resp !== false) {
          this.institutionArray = resp;





        }
      }
    )
  }
  getTypeUsers() {
    this.userService.listTypeUser().subscribe(
      resp => {


        if (resp !== false) {

          this.typeUsersArray = resp;

        }
      }
    )
  }
  async user() {

    let object = {
      // documentId: this.myForm.value.documentId,
      email: this.myForm.value.email,
      documentType: this.myForm.value.documentType,
      emailCorporate: this.myForm.value.emailCorporate,
      state: this.myForm.value.state,
      role: this.myForm.value.idRole,
      typeUser: this.myForm.value.typeUser,
      institution: this.myForm.value.idInstitution,

    }

    if (this.myForm.invalid === true) {
      this.myForm.markAllAsTouched();
      this.errorHandlingService.negativeAnswer('Ingrese los datos requeridos')
    } else {

      this.userService.putUser(
        object
      ).subscribe(
        resp => {
          if (resp !== false) {
            this.errorHandlingService.positiveAnswer('Usuario actualizado')

          } else {
            this.errorHandlingService.negativeAnswer('Revise  los datos')

          }
        }
      );
    }



  }

  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  errorMsgForm(nameFormValue: string, name: string): string {
    const errors = this.myForm.get(nameFormValue)?.errors;
    if (errors) {

      return ` ${name} ${this.errorHandlingService.name(errors)}`;
    }
  }
}
