import { Component } from '@angular/core';
import { SpinnerService } from '../../service/spinner.service';
@Component({
  selector: 'app-spineer',
  template: `
  <div class="overlay" *ngIf="isLoading$ | async">
        <div class="loadingio-spinner-rolling-zvsd6sfg6e"><div class="ldio-ije64jmhzm">
        <div></div>
        </div></div>
  </div>
  `,
  styleUrls: ['./spineer.component.css'],
})
export class SpineerComponent   {
isLoading$=this.spinnerService.isLoading$;
  constructor(private spinnerService:SpinnerService) { }
  ngOnInit(): void {
  }
}
