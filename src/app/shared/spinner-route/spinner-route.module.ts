import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerRouteComponent } from './spinner-route.component';



@NgModule({
  declarations: [
    SpinnerRouteComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    SpinnerRouteComponent
  ]
})
export class SpinnerRouteModule { }
