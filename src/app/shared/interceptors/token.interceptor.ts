
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError  } from 'rxjs/operators';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TokenService implements HttpInterceptor {

  constructor(
    private router: Router) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   
    
    let request = req;
    if (localStorage.getItem('x-tq')) {
      request = req.clone({
        setHeaders: {
          authorization: `Bearer ${localStorage.getItem('x-tq')}`
        }
      });
    }

    return next.handle(request).pipe(

      catchError((err: HttpErrorResponse) => {


        if (err.status === 401) {
          
          this.router.navigateByUrl('/login');
          localStorage.clear();
          Swal.fire({
            icon: 'error',
            text: 'Sesion caducada',
            confirmButtonText: 'Aceptar',
            customClass: {
              popup: 'format-pre'
            },
            timer: 2500
          });
        
        }
        if (err.status === 403) {
          
          this.router.navigateByUrl('/session/dashboard');
 
          Swal.fire({
            icon: 'warning',
            text: 'Permisos insuficientes',
            confirmButtonText: 'Aceptar',
            customClass: {
              popup: 'format-pre'
            },
            timer: 2500
          });
        
        }

        return throwError(err);

      })
    );
  }
}
