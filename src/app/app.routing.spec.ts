

import { AppComponent } from './app.component';
import { TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { AppRoutes } from './app.routing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouterLink, RouterLinkWithHref, RouterOutlet } from '@angular/router';

describe('app.component',()=>{

    beforeEach(async () => {
        await TestBed.configureTestingModule({
          imports: [
            RouterTestingModule.withRoutes([])
          ],
          declarations: [
            AppComponent
          ],
          schemas:[NO_ERRORS_SCHEMA]
        }).compileComponents();
      });
    it(`Debe de tener un router outlet `, () => {
        const fixture = TestBed.createComponent(AppComponent);
        const debugElement= fixture.debugElement.query(By.directive(RouterOutlet));
        expect(debugElement).not.toBeNull();
      });
    it('Debe contener la ruta de sesión usuario "session "',()=>{
        expect( AppRoutes[1]?.path).toEqual('session');
    })
    it('Debe contener la ruta de registro usuario "login"',()=>{
        expect( AppRoutes[2]?.path).toEqual('login');
    })
    it('Debe contener la ruta de registro usuario "register"',()=>{
        expect( AppRoutes[3]?.path).toEqual('register');
    })
    it('Debe contener la ruta de registro usuario "qr "',()=>{
        expect( AppRoutes[4]?.path).toEqual('qr');
    })
    it('Debe contener la ruta de registro usuario "recovery-password"',()=>{
        expect( AppRoutes[5]?.path).toEqual('recovery-password');
    })

    
})