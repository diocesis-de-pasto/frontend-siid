import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  ChildActivationStart,
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  ResolveEnd,
  Router
} from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class AppComponent implements OnInit {
  private _router: Subscription;
  title = 'Ng-Teams';
  loading = false;
  constructor( private router: Router ) {


    this.router.events.subscribe((event: Event) => {
      switch (true) {


        case event instanceof NavigationStart:{
          this.loading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError:
        case event instanceof   ResolveEnd:
     {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });

  }

    ngOnInit() {
      this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
        const body = document.getElementsByTagName('body')[0];
        const modalBackdrop = document.getElementsByClassName('modal-backdrop')[0];
        if (body.classList.contains('modal-open')) {
          body.classList.remove('modal-open');
          modalBackdrop.remove();
        }
      });
    }
}
