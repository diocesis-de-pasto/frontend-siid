import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LockGuard } from '../auth/guards/lock.guard';
import { RegisterUserHomeComponent } from './register-user-home/register-user-home.component';
const routes: Routes = [
  {
    path: '',
    children: [{
        path: '',
        component: RegisterUserHomeComponent
    }] 
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterUserRoutingModule { }
