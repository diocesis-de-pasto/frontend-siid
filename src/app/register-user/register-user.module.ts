import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterUserRoutingModule } from './register-user.routing';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterUserHomeComponent } from './register-user-home/register-user-home.component';
@NgModule({
  declarations: [RegisterUserHomeComponent],
  imports: [
    CommonModule,
    RegisterUserRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class RegisterUserModule { }
