import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient,
    private router: Router) { }
  register(email: string, documentId: string, documentType: string, nameInstitution: number, nameTypeUser: number) {
    const url = `${this.baseUrl}/registro-cedula-email`;
    const body = { email, documentId, documentType, nameInstitution, nameTypeUser };
    return this.http.post<any>(url, body)
      .pipe(
         map(resp=>{
            if(resp.status===200){
              return true;
            }else{
              return false;
            }
         }),
         catchError(err => of(err.ok = false))
      )
  }
}
