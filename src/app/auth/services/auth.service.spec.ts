import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpErrorResponse } from '@angular/common/http';

describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
    });
    service = TestBed.inject(AuthService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });



  it('Usuario logueado', () => {
    const expectedResponse = true;

    const responseObject = {
      mensaje: 'Usuario logueado'
    };

    service.login('dzambrano863@gmail.com', 'daniel').subscribe((res) => {
      expect(res).toEqual(expectedResponse);
    });

    const req = httpMock.expectOne(
      'https://back.diocesisdepasto.org/login'
    );

    req.flush(responseObject);
  });

  it('Peticion de logueo fallida', (done: DoneFn) => {
    const expectedResponse = false;
    service.login('dzambrano863@gmail.com', 'daniel').subscribe(
      error => {
        expect(error).toEqual(expectedResponse);
        done()
      }
    );
    const req = httpMock.expectOne(
      'https://back.diocesisdepasto.org/login'
    );

    req.flush({}, { status: 400, statusText: 'Bad Request' });
  });
});
