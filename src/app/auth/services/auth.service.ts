import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { of, timer } from 'rxjs';
import { Router } from '@angular/router';
import { catchError, map, tap } from "rxjs/operators"; //Operador que permite mutar la respuesta en el   
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _usuario!: any;
  private baseUrl: string = environment.baseUrl;
  inscribir: any;
  inactivo: string;
  private httpClient: HttpClient;
  constructor(
    private router: Router,
    httpBackend: HttpBackend
  ) { this.httpClient = new HttpClient(httpBackend); }
  get usuario() {
    return { ...this._usuario }
  }
  login(email: string, password: string) {
    const url = `${this.baseUrl}/login`;
    const body = { email, password };


    return this.httpClient.post<any>(url, body)
      .pipe(
        tap(resp => {
          if (resp.access_token) {
            localStorage.setItem('x-tq', resp.access_token!)
            localStorage.setItem('ito', 'F')
          }
        }),
        map(resp => {
          return resp.ok = true
        }),
       catchError(err => {
         return of(err.ok = false)
       })
      )
  }
  validarToken() {
    const url = `${this.baseUrl}/login/validateToken`;
    const headers = new HttpHeaders()
      .set('Authorization', ("Bearer " + localStorage.getItem('x-tq')) || '');
    return this.httpClient.get<any>(url, { headers })
      .pipe(
        map(resp => {
          if (resp.message === true) {
            return true
          }
        }),
        catchError(err => of(false))
      )
  }
  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }
  movimiento() {
    if (localStorage.getItem('ito') === 'F') {
      this.empezar();
      let mov = document.body.onmousemove = (e: any) => {
        if (localStorage.getItem('ito') === 'F') {
          this.onDetener();
          this.empezar();
        }
      }
    }
  }
  onDetener() {

    this.inscribir.unsubscribe();
  }
  async empezar() {
    let source = await timer(1800000);
    this.inscribir = source.subscribe(val => {
      if (localStorage.getItem('ito') === 'F') {
        localStorage.setItem('ito', 'V')
        this.router.navigateByUrl('/login/lock');
      }
    });
  }

  errorsEmail(err:any){

    if (err?.required) {
      return 'Email es obligatorio'
    } else if (err?.pattern) {
      return 'El valor ingresado no tiene formato de correo'
    }
    return ''
  }
}
