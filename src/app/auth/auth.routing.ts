import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginGuard } from './guards/login.guard';
import { LockComponent } from './lock/lock.component';
import { LockGuard } from './guards/lock.guard';
export const AuthRoutes: Routes = [
    {
        path: '',
        children: [ {
            path: '',
            component: LoginComponent
        }],
        canActivate:[LoginGuard],
        canLoad:[LoginGuard]
    },
    {
        path: 'lock',
        children: [{
            path: '',
            component: LockComponent
        }] ,
        canActivate:[LockGuard],
        canLoad:[LockGuard]
    }
];
