import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import jwt_decode from 'jwt-decode';
import { AuthService } from '../services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GlobalService } from '../../service/global.service';
@Component({
    selector: 'app-lock-cmp',
    templateUrl: './lock.component.html',
    styleUrls: ['./lock.component.css']
})
export class LockComponent implements OnInit {
  public fieldTextType: boolean;
  public repeatFieldTextType: boolean;
  public test: Date = new Date();
  public myForm: FormGroup = new FormGroup({});
  public mail:string;
    constructor(private authService:AuthService,
      private router: Router,
      private fb: FormBuilder,
      private _snackBar: MatSnackBar,
      private globalService:GlobalService,){
    }

    ngOnInit() {
      this.mail=this.globalService.userEmail;
      this.myForm = this.fb.group({
        password: ['', [Validators.required]]
      });
    }
    get email(): string {
    let {email}= this.getDecodedAccessToken();
      return email;
    }
    login() {
      const {password} = this.myForm.value;
      this.authService.login(this.email, password)
        .subscribe(resp => {
          if (resp) {
            localStorage.setItem('ito','F');
            this.router.navigateByUrl('/session/dashboard');
              this.correct(); 
          } else {
            this.incorrect();
          }
        });
    }
    getDecodedAccessToken(): any {
      try{
          return jwt_decode(localStorage.getItem('x-tq'));
      }
      catch(Error){
          return null;
      }
    }
    campoNoValido(campo: string) {
      return this.myForm.get(campo)?.invalid
        && this.myForm.get(campo)?.touched
    }
  
    correct() {
      this._snackBar.open("Acceso correcto, !Bienvenido!", "Aceptar", {
        duration: 4000,
        horizontalPosition: "right",
        verticalPosition: 'bottom',
      });
    }
    incorrect() {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'El usuario o contraseña son incorrectos.',
        confirmButtonText: 'Aceptar',
        customClass: {
          popup: 'format-pre'
        }
      });
    }
    logout(){
      this.authService.logout();
    }
    toggleFieldTextType() {
      this.fieldTextType = !this.fieldTextType;
    }
    toggleRepeatFieldTextType() {
      this.repeatFieldTextType = !this.repeatFieldTextType;
    }
}
