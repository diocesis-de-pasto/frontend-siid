import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LockGuard implements CanActivate, CanLoad {
  constructor(private authService: AuthService,
    private router: Router) { }

  canActivate(): Observable<boolean> | boolean {


    return this.revision();
  }
  canLoad(): Observable<boolean> | boolean {

    return this.revision();
  }

  
  revision(){ 


    return  this.authService.validarToken().pipe(

        map((resp) => {

          if(resp===false){
            this.router.navigateByUrl('/login')
             return false;
          }
          if(resp && (localStorage.getItem('ito')==='F' || !localStorage.getItem('ito'))){
             this.router.navigateByUrl('/session/dashboard')
             return false;
          }
          return true;
        }),
   
    );
   } 
}
