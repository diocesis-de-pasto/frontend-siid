import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from "rxjs/operators";
import { AuthService } from '../services/auth.service';
import { HttpBackend, HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class ValidateTokenGuard implements CanActivate {
  private httpClient: HttpClient;

  constructor(private authService: AuthService,
    private router: Router,  httpBackend: HttpBackend
    ) { 
    this.httpClient = new HttpClient(httpBackend);

    }

  canActivate(): Observable<boolean> | boolean {



    return this.revision();
  }


  canActivateChild( state: RouterStateSnapshot): Observable<boolean> | boolean {
    
    return this.revision();
  }

  revision() {


    return this.authService.validarToken().pipe(
      tap(valid => {
        
        if (!valid) {

          this.router.navigateByUrl('/login')
          return false;
        }
        if (valid===true && (localStorage.getItem('ito') === 'V')) {

          this.router.navigateByUrl('/login/lock')
          return false;
        }
        else {

          return true
        }
      })
    );
  }
}
