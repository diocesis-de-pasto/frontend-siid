import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { InjectTokenService } from '../../sidebar/services/inject-token.service';

@Injectable({
  providedIn: 'root'
})
export class ValidateComponentsGuard implements CanActivate, CanActivateChild {
  constructor(private injectTokenService: InjectTokenService,
    private router: Router,) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {


    return this.condition(state);
  }


  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

    return this.condition(state);
  }

  condition(state: RouterStateSnapshot){
    if(localStorage.getItem('x-tq')){
      var condition=this.injectTokenService.componentAcces(state.url);
 
      
      return condition;
    }else{
      localStorage.clear();
      this.router.navigateByUrl('/login');
      return false;
    }
  }

}
