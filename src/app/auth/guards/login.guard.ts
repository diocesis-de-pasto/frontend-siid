import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { logging } from 'selenium-webdriver';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate, CanLoad {

  constructor(private authService: AuthService,
    private router: Router) { }

    

  canActivate(): Observable<boolean> | boolean {


    return this.revision();
  }
  canLoad(): Observable<boolean> | boolean {

    return this.revision();
  }


  revision() {


    
    if ((localStorage.getItem('x-tq') || localStorage.getItem('ito') === 'F')) {
      
      return this.authService.validarToken().pipe(
        map((resp) => {

          if (resp === true) {
            this.router.navigateByUrl('/session/dashboard')
            
            return false;
          } else {
            return true;
          }

        }),

      );

    } else {

      return true;
    }


  }
}
