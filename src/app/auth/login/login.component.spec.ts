import { ComponentFixture, TestBed } from '@angular/core/testing';


import { RouterLinkWithHref } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../../app.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [ReactiveFormsModule, FormsModule, HttpClientModule, MaterialModule, BrowserAnimationsModule, RouterTestingModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;

  });
  it(`Debe de tener un link a la ruta "'/recovery-password/window/password'" en el DOM`, () => {
    const elementos = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref))
    let existe = false;
    for (const elem of elementos) {
      if (elem.attributes['routerLink'] === '/recovery-password/window/password') {
        existe = true;
        break;
      }


    }
    expect(existe).toBeTruthy();

  });

  it('Debe mostrar la leyenda "Sistema Integrado de Información Diocesana"', () => {
    fixture.detectChanges();
    const elem: HTMLElement = fixture.debugElement.nativeElement.querySelector('#TEXT_PRIMARY');
    expect(elem.innerHTML).toContain('Sistema Integrado de Información Diocesana');
  });
  it('Debe mostrar la leyenda "Diócesis de Pasto"', () => {
    fixture.detectChanges();
    const elem: HTMLElement = fixture.debugElement.nativeElement.querySelector('#TEXT_SECONDARY');
    expect(elem.innerHTML).toContain('Diócesis de Pasto');
  });

  it('Debe mostrar la leyenda "Bienvenid@"', () => {
    fixture.detectChanges();
    const elem: HTMLElement = fixture.debugElement.nativeElement.querySelector('#WELCOME_TITLE');
    expect(elem.innerHTML).toContain('Bienvenid@');
  });

});
