import { Component, ChangeDetectorRef, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { NameService } from '../../service/name.service';
import { ErrorHandlingService } from '../../service/error-handling.service';
import { LoginConstants } from '../../service/utils/login-constants';
declare var $: any;
@Component({
  selector: 'app-login-cmp',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public CONST = LoginConstants;
  public varSpinner: boolean = false;
  public fieldTextType: boolean;
  public repeatFieldTextType: boolean;
  public myForm: FormGroup = new FormGroup({});
  private httpClient: HttpClient;
  constructor(private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private nameService: NameService,
    private _snackBar: MatSnackBar, private errorHandlingService: ErrorHandlingService,
    httpBackend: HttpBackend
  ) {
    this.httpClient = new HttpClient(httpBackend);
  }
  ngOnInit() {
    localStorage.clear();
    this.myForm = this.fb.group({
      email: ["", [Validators.required, Validators.pattern(this.nameService.emailPattern)]],
      password: ["", [Validators.required]]
    });
  }
   recoveryPassword() {
      this.router.navigateByUrl('/recovery-password/window/password');
  }
  login() {
    this.varSpinner = true;
    const { email, password } = this.myForm.value;
    this.authService.login(email, password)
      .subscribe(resp => {
        if (resp) {
          this.router.navigateByUrl('/session/dashboard');
          this.varSpinner = false;
          this.correct();
        } else {
          this.varSpinner = false;
          this.incorrect();
        }
      });
  }
  get emailErrorMsg(): string {
    const errors = this.myForm.get('email')?.errors;
    return this.errorHandlingService.errorsEmail(errors);

  }
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleRepeatFieldTextType() {
    this.repeatFieldTextType = !this.repeatFieldTextType;
  }
  correct() {
    this._snackBar.open("Acceso correcto, !Bienvenido!", "Aceptar", {
      duration: 4000,
      horizontalPosition: "right",
      verticalPosition: 'bottom',
    });
  }
  incorrect() {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: 'El correo o contraseña son incorrectos.',
      confirmButtonText: 'Aceptar',
      customClass: {
        popup: 'format-pre'
      }
    });
  }
}
