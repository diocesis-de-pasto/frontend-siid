import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map, catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PasswordService {
  private baseUrl: string = environment.baseUrl;
  constructor(
    private router: Router,
    private httpClient:HttpClient,
  ) {  }
  validateEmail(email: string) {
    const url = `${this.baseUrl}/recovery-password`;
    const body = { email };
    return this.httpClient.post<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(err => of(false))
      );
  }
  validateToken(token: string) {
    const url = `${this.baseUrl}/recovery-password/validationToken/${token}`;
    return this.httpClient.get<any>(url)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(err => of(false))
      );
  }
  recoveryPass(
    token: string, passwordUser: string
  ) {
    const url = `${this.baseUrl}/recovery-password/${token}`;
    const body = {
      password: passwordUser
    };
    return this.httpClient.post<any>(url,body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(err => of(true))
      );
  }
}
