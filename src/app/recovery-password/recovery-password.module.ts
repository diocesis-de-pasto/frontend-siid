import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecoveryPasswordRoutingModule } from './recovery-password-routing.module';
import { RecoveryPassComponent } from './recovery-pass/recovery-pass.component';
import { RecoveryWindowComponent } from './recovery-window/recovery-window.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
@NgModule({
  declarations: [
    RecoveryPassComponent,
    RecoveryWindowComponent
  ],
  imports: [
    CommonModule,
    RecoveryPasswordRoutingModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class RecoveryPasswordModule { }
