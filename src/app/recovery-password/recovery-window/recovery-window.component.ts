import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import jwt_decode from 'jwt-decode';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GlobalService } from '../../service/global.service';
import { NameService } from 'src/app/service/name.service';
import { PasswordService } from '../service/password.service';
import { ErrorHandlingService } from '../../service/error-handling.service';
@Component({
  selector: 'app-recovery-window',
  templateUrl: './recovery-window.component.html',
  styleUrls: ['./recovery-window.component.css']
})
export class RecoveryWindowComponent implements OnInit {
  varSpinner: boolean = false;
  constructor(
    private nameService: NameService,
    private router: Router,
    private fb: FormBuilder,
    private passwordService: PasswordService,
    private errorHandlingService: ErrorHandlingService,) {
  }
  myForm: FormGroup = new FormGroup({});
  ngOnInit(): void {
    this.myForm = this.fb.group({
      email: [, [Validators.required, Validators.pattern(this.nameService.emailPattern)]],
    });
  }
  get emailErrorMsg(): string {
    const errors = this.myForm.get('email')?.errors;
    if (errors?.required) {
      return 'Email es obligatorio'
    } else if (errors?.pattern) {
      return 'El valor ingresado no tiene formato de correo'
    }
    return ''
  }
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  async salir() {
    this.varSpinner = true;
    var petition = await this.errorHandlingService.question('¿Salir?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'SI', 'NO', 'question');
    if (petition === true) {
      this.router.navigateByUrl('/login');
      this.varSpinner = false;
    }else{
      this.varSpinner = false;
    }
    this.varSpinner = false;
  }
  async check() {
 
      this.varSpinner = true;
      const { email } = this.myForm.value;
      this.passwordService.validateEmail(email)
        .subscribe(resp => {
          if (resp === true) {
            this.varSpinner = false;
            this.errorHandlingService.positiveAnswer('Correo enviado correctamente, revise su correo')
            this.router.navigateByUrl('/login')
          } else {
            this.varSpinner = false;
            this.errorHandlingService.negativeAnswer('El correo no existe.')
          }
        });
    
  }
}
