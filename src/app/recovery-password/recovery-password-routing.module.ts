import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecoveryWindowComponent } from './recovery-window/recovery-window.component';
import { RecoveryPassComponent } from './recovery-pass/recovery-pass.component';
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: ':token',
        component: RecoveryPassComponent
      },
      {
        path: 'window/password',
         component: RecoveryWindowComponent
      },
      {
        path:"**",
        redirectTo: '/login',
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecoveryPasswordRoutingModule { }
