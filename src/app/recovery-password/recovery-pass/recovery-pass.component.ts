import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { PasswordService } from '../service/password.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-recovery-pass',
  templateUrl: './recovery-pass.component.html',
  styleUrls: ['./recovery-pass.component.css']
})
export class RecoveryPassComponent implements OnInit {
  token: string;
  fieldTextType: boolean;
  repeatFieldTextType: boolean;
  fieldTextType2: boolean;
  repeatFieldTextType2: boolean;
  myForm: FormGroup = new FormGroup({});
  text: string;
  flag: boolean = false;
  constructor(private fb: FormBuilder,
    private router: Router,
     private passwordService: PasswordService,
    private errorHandlingService: ErrorHandlingService,
    private activatedRoute: ActivatedRoute) { }
  ngOnInit(): void {
    this.token = this.activatedRoute.snapshot.params.token;
    if (this.token) {
      this.passwordService.validateToken(this.token).subscribe(
        resp => {
          if (resp === true) {
            this.flag = true;
          } else {
            this.errorHandlingService.negativeAnswer('Error')
            this.router.navigateByUrl('/login')
          }
        }
      )
    }



    this.myForm = this.fb.group({
      password: [, [Validators.required]],
      passwordtwo: [, [Validators.required]],
    });
  }
  validatePasswords() {
    if (this.myForm.value.password === this.myForm.value.passwordtwo) {
      if (!this.myForm.value.password || !this.myForm.value.passwordtwo) {
        this.text = "Las contraseñas deben coincidir"
        return true
      } else {
        return false
      }
    }
    else {
      this.text = "Las contraseñas deben coincidir"
      return true;
    }
  }
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleRepeatFieldTextType() {
    this.repeatFieldTextType = !this.repeatFieldTextType;
  }
  toggleFieldTextType2() {
    this.fieldTextType2 = !this.fieldTextType2;
  }
  toggleRepeatFieldTextType2() {
    this.repeatFieldTextType2 = !this.repeatFieldTextType2;
  }
  async actualizar() {
    const { password } = this.myForm.value;
    this.passwordService.recoveryPass(this.activatedRoute.snapshot.params.token, password)
      .subscribe(resp => {
        if (resp === true) {
          this.errorHandlingService.positiveAnswer('Contraseña actualizada, inicie sesión')
          this.reset()
          this.router.navigateByUrl('/login')
        } else {
          this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          this.myForm.reset();
        }
      });
  }
  reset() {
    this.myForm.reset();
    this.myForm.markAsPristine;
    this.myForm.markAsTouched;
  }
}
