
import { routes } from './user.routing';
import { ListUserComponent } from './components/list-user/list-user.component';
import { ViewCreateEditComponent } from '../module-diocesano/pages/view-create-edit/view-create-edit.component';
import { TestBed } from '@angular/core/testing';
import { UserComponent } from '../user-diocesano/user/user.component';
import { By } from '@angular/platform-browser';
import { RouterOutlet } from '@angular/router';
describe('UserComponent',()=>{    

    xit(`Debe de tener un router outlet `, () => {
        const fixture = TestBed.createComponent(UserComponent);
        const debugElement= fixture.debugElement.query(By.directive(RouterOutlet));
        expect(debugElement).not.toBeNull();
      });
    it('Debe contener la ruta  "principal"',()=>{
        expect(routes[0]?.path).toEqual('principal')
    });
    it('Debe contener la ruta  "create"',()=>{
        expect(routes[1]?.path).toEqual('create')
    });
    it('Debe contener la ruta  "edit/:id"',()=>{
        expect(routes[2]?.path).toEqual('edit/:id')
    });
    it('Debe contener la ruta  "create"',()=>{
        expect(routes[3]?.path).toEqual('view/:id')
    });
})