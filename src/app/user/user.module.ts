import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user.routing';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListUserComponent } from './components/list-user/list-user.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { ComponentsUserModule } from '../shared/components-user/components-user.module';

import { ViewCreateEditComponent } from './pages/view-create-edit/view-create-edit.component';
import { FastRegisterComponent } from './components/fast-register/fast-register.component';


@NgModule({
  declarations: [
 


     ListUserComponent,
     EditUserComponent,
     CreateUserComponent,

     ViewCreateEditComponent,
       FastRegisterComponent


  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsUserModule
  ]
})
export class UserModule { }
