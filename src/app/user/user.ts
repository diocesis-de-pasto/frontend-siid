export interface User {
    idUser: number,
    nickname: string,
    email: string,
    emailCorporate: string,
    state: string
    documentId: string,
    nameTypeUser: string,
    documentType:string


}
