import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListUserComponent } from './components/list-user/list-user.component';
import { ViewCreateEditComponent } from './pages/view-create-edit/view-create-edit.component';

export const routes: Routes = [
      { path: 'principal',  component: ListUserComponent}, 
      { path: 'create', component: ViewCreateEditComponent},
      { path: 'edit/:id', component: ViewCreateEditComponent}, 
      { path: 'view/:id', component: ViewCreateEditComponent},
      { path: '**', redirectTo: '/principal'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}

