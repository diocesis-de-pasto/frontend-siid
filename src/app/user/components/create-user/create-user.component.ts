import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErrorHandlingService } from '../../../service/error-handling.service';
import { RoleService } from '../../../role-diocesano/service/role.service';


import { NameService } from '../../../service/name.service';
import { InjectTokenService } from '../../../sidebar/services/inject-token.service';
import { UserService } from 'src/app/shared/components-user/components/user-information/user.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['../../../css/formulario.css'],
  encapsulation: ViewEncapsulation.None,
})
export class CreateUserComponent implements OnInit {




  public typeDocument = ['C.C', 'TARJETA DE IDENTIDAD', 'PASAPORTE', 'DOCUMENTO EXTRANGERO'
  ];

  public roleName: any[] = [];
  public rol: CrudPermiss;
  public institution: CrudPermiss;
  public typeUser: CrudPermiss;
  @Input() permissCrud: any = [];
  public myForm: FormGroup = new FormGroup({});

  public rolArray = [];
  public typeUsersArray = [];
  public institutionArray = [];

  constructor(private fb: FormBuilder,
    private errorHandlingService: ErrorHandlingService,
    private userService: UserService,
    private roleService: RoleService,
    private nameService: NameService,
    private injectTokenService: InjectTokenService

  ) {

    this.myForm = this.fb.group({
      documentId: [, [Validators.required, Validators.pattern(this.nameService.numberPattern), Validators.maxLength(20)]],
      documentType: [, [Validators.required]],
      email: [, [Validators.required, Validators.pattern(this.nameService.emailPattern), Validators.maxLength(100)]],
      role: [, [Validators.required]],
      typeUser: [, [Validators.required]],
      idInstitution: [, [Validators.required]],
      emailCorporate: [, [Validators.pattern(this.nameService.emailPattern), Validators.maxLength(100)]],

    });
  }
  ngOnInit() {

    this.rol = this.injectTokenService.permission('Administración de roles');
    this.institution = this.injectTokenService.permission('Administración instituciones');
    this.typeUser = this.injectTokenService.permission('Administración de tipos de usuario');
  
    this.getRol();
    this.getInstituion();
    this.getTypeUsers();
    this.myForm.reset();
  }

  getRol() {
    this.roleService.listRole().subscribe(
      resp => {

        if (resp !== false) {
          this.rolArray = resp;
        }
      }
    )
  }
  getInstituion() {
    this.userService.listInstitution().subscribe(
      resp => {
        if (resp !== false) {
          this.institutionArray = resp;





        }
      }
    )
  }
  getTypeUsers() {
    this.userService.listTypeUser().subscribe(
      resp => {


        if (resp !== false) {

          this.typeUsersArray = resp;

        }
      }
    )
  }

  verificatePersonal() {
    return true;
  }
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }

  errorMsgForm(nameFormValue: string, name: string): string {
    const errors = this.myForm.get(nameFormValue)?.errors;
    if (errors) {
      return `Error "${name}" ${this.errorHandlingService.name(errors)}`;
    }
  }
  reset() {
    this.myForm.reset();
    this.myForm.value.refModule = 0;
  }
  async createUser() {


    let object = {
      documentId: this.myForm.value.documentId,
      email: this.myForm.value.email,
      documentType: this.myForm.value.documentType,
      emailCorporate: this.myForm.value.emailCorporate,
      state: this.myForm.value.state,
      role: this.myForm.value.role,
      typeUser: this.myForm.value.typeUser,
      institution: this.myForm.value.idInstitution,

    }


    if (this.myForm.invalid === true) {
      this.myForm.markAllAsTouched();
      this.errorHandlingService.negativeAnswer('Ingrese los datos requeridos')
    } else {
      var petition = await this.errorHandlingService.question('¿Añadir el usuario?',
        'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'SI', 'NO', 'question');
      if (petition === true) {
        this.userService.createUser(
          object
        ).subscribe(
          resp => {
            if (resp !== false) {
              this.errorHandlingService.positiveAnswer('Usuario registrado')
              this.reset();
            } else {
              this.errorHandlingService.negativeAnswer('Revise  los datos')

            }
          }
        );
      }
    }
  }
}
