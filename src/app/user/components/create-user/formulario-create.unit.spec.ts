
import { FormBuilder } from '@angular/forms';
import { CreateUserComponent } from './create-user.component';
import { ErrorHandlingService } from '../../../service/error-handling.service';
import { NameService } from '../../../service/name.service';
describe('Formulario crear usuario unit test',()=>{
  
    let componente:CreateUserComponent;

    beforeEach(()=>{
        componente = new CreateUserComponent(
            new FormBuilder(),
             new ErrorHandlingService(),
            null,
             null,
             new NameService(),null);

    })

    it('Es necesario realizar un formulario con ocho campos: documento, tipo de documento, contraseña, correo principal, rol, tipo de usuario, institucion y email corporativo',()=>{


        expect(componente.myForm.contains('documentId')).toBeTruthy();
        expect(componente.myForm.contains('documentType')).toBeTruthy();
        expect(componente.myForm.contains('email')).toBeTruthy();
        expect(componente.myForm.contains('role')).toBeTruthy();
        expect(componente.myForm.contains('typeUser')).toBeTruthy();
        expect(componente.myForm.contains('idInstitution')).toBeTruthy();
        expect(componente.myForm.contains('emailCorporate')).toBeTruthy();

    });

    it('El email principal debe  ser obligatorio',()=>{
        const control = componente.myForm.get('email');
        control?.setValue('');
        expect(control?.valid).toBeFalsy();
      })


    it('El email debe ser un correo valido',()=>{
        const control = componente.myForm.get('email');
        control?.setValue('daniel@gmail.com');
        expect(control?.valid).toBeTruthy();
      })
  
  



}  )