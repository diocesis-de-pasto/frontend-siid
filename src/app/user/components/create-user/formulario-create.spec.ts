import { TestBed, ComponentFixture } from '@angular/core/testing';
import { CreateUserComponent } from './create-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http'; 
import { MaterialModule } from '../../../app.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
describe('Formulario crear usuario integrate test', () => {

    let component: CreateUserComponent;
    let fixture: ComponentFixture<CreateUserComponent>;

    beforeEach( () => {
        TestBed.configureTestingModule({
            declarations: [ CreateUserComponent ],
            imports: [ReactiveFormsModule, FormsModule,HttpClientModule,MaterialModule,BrowserAnimationsModule,RouterTestingModule]
            
        });
        fixture = TestBed.createComponent(CreateUserComponent);
        component = fixture.componentInstance;

    });

    it('Cantidad de elementos representados en la interfaz de usuario es igual a la de los formularios reactivos', () => {

        const elem:HTMLElement = fixture.debugElement.nativeElement.querySelector('#registerUserForm');
        const inputElements = elem.querySelectorAll('input')        
         expect(inputElements.length).toEqual(3);


    });
    it('Valores iniciales de formulario registrar usuario', () => {

        const registerUserFormGroup = component.myForm;
        const registerUserFormValues={
            documentId: null,
            documentType:null ,
            email:null ,
            role: null,
            typeUser: null,
            idInstitution: null,
            emailCorporate: null
        }
        expect(registerUserFormGroup.value).toEqual(registerUserFormValues);
    });
    xit('Valores iniciales de formulario registrar usuario', () => {

        const registerUserForm:HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#registerUserForm').querySelectorAll('input')[0];
        
        
        registerUserForm.value = '31231231';
        registerUserForm.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(()=>{
   
            
            const idFormGroup= component.myForm.get('documentId');
     

            expect(registerUserForm.value).toEqual('idFormGroup.value');
            expect(idFormGroup.errors).toBeNull();

        });
        
    });
    // it('Debe de mostrar en el input el valor del progreso', () => {
    //     component.cambiarValor(5);

    //     fixture.detectChanges();//Disparar la deteccion  de cambios

    //     fixture.whenStable().then(()=>{
    //         const input = fixture.debugElement.query(By.css('input'));
    //         const elem = input.nativeElement;
            
    //         expect(elem.value).toBe('55');
    
    
    //     })

    // });
    // it('Debe de incrementar/decrementar en 5, con un click en el boton', () => {



    //     const botones= fixture.debugElement.queryAll(By.css('.btn-primary'))

    //     botones[0].triggerEventHandler('click',null);

    //     expect(component.progreso).toBe(45)

    //     botones[1].triggerEventHandler('click',null);

    //     expect(component.progreso).toBe(50)


    // });
    // it('En  el titulo del componente debe mostrar el progreso', () => {

    //     const botones= fixture.debugElement.queryAll(By.css('.btn-primary'))

    //     botones[0].triggerEventHandler('click',null);

    //     fixture.detectChanges();
    //     const elem:HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;
        
    //     expect(elem.innerHTML).toContain('45');


    // });



});
