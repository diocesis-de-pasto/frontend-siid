import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudPermiss } from 'src/app/interface/crud-permiss';
import { InjectTokenService } from '../../../sidebar/services/inject-token.service';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  public idUser: string;
  public crudUser: CrudPermiss ;
  public crudDataPerson: CrudPermiss ;
  public crudMenisterial: CrudPermiss ;
  public crudAcademy: CrudPermiss ;
  public crudPastoral: CrudPermiss ;
  public crudRecognition: CrudPermiss ;
  public crudFamily: CrudPermiss ;

  @Input() view: boolean =false;
  constructor(
    private activatedRoute:ActivatedRoute,private injectTokenService:InjectTokenService) { }

  permiss(){

    
    this.crudUser ={
      admin: 1,
      create: 1,
      read: 1,
      update: 1,
      delete: 1
    }
    this.crudDataPerson = {
      admin: 1,
      create: 1,
      read: 1,
      update: 1,
      delete: 1
    }
    this.crudAcademy      = this.injectTokenService.permission('Administración de datos académicos');
    this.crudMenisterial = this.injectTokenService.permission('Administración información ministerial');
    this.crudPastoral = this.injectTokenService.permission('Administración de datos pastorales');
    this.crudRecognition = this.injectTokenService.permission('Administración de reconocimientos');
    this.crudFamily = this.injectTokenService.permission('Administración contactos familiares');

  }
  ngOnInit(): void {
    this.permiss();
    if(this.activatedRoute.snapshot.params.id){
      this.idUser = this.activatedRoute.snapshot.params.id;
    };
  }
  step = 0;
  ok;
  setStep(index: number) {
     this.step = index;
  }
  nextStep() {
     this.step++;
  }
  prevStep() {
     this.step--;
  }
  getUser(){
  }
}
