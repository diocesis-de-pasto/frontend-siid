import { Component, ChangeDetectorRef, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpBackend, HttpClient } from '@angular/common/http';



import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { LoginConstants } from 'src/app/service/utils/login-constants';
import { NameService } from 'src/app/service/name.service';
import { RegisterService } from 'src/app/register-user/service/register.service';

@Component({
  selector: 'app-fast-register',
  templateUrl: './fast-register.component.html',
  styleUrls: ['./fast-register.component.css']
})
export class FastRegisterComponent implements OnInit {

  public CONST = LoginConstants;
  fieldTextType: boolean;
  repeatFieldTextType: boolean;
  myForm: FormGroup = new FormGroup({});
  private httpClient: HttpClient;
  constructor(private fb: FormBuilder,
    private router: Router,
    private nameService: NameService,
    private registerService: RegisterService,
    private errorHandlingService: ErrorHandlingService,
    httpBackend: HttpBackend
  ) {
    this.httpClient = new HttpClient(httpBackend);
  }
  get emailErrorMsg(): string {
    const errors = this.myForm.get('email')?.errors;
    if (errors?.required) {
      return 'Email es obligatorio'
    } else if (errors?.pattern) {
      return 'El valor ingresado no tiene formato de correo'
    }
    return ''
  }
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleRepeatFieldTextType() {
    this.repeatFieldTextType = !this.repeatFieldTextType;
  }
  ngOnInit() {
    this.myForm = this.fb.group({
      id: [, [Validators.required, Validators.pattern("^[0-9]*$")]],
      email: [, [Validators.required, Validators.pattern(this.nameService.emailPattern)]],
    });
  }
  get idErrorMsg(): string {
    const errors = this.myForm.get('id')?.errors;
    if (errors?.required) {
      return 'La identificacion es obligatoria'
    } else if (errors?.pattern) {
      return 'La indentifacion solo debe contener numeros'
    }
    return ''
  }
  sesion() {
    this.router.navigate(['/login']);
  }
  reset() {
    this.myForm.reset();
    this.myForm.markAsPristine;
    this.myForm.markAsTouched;
  }
  async register() {
    const { email, id } = this.myForm.value;
    var correo = `<b>Correo</b> \n${email}`;
    var identification = `<b>Identificación</b> \n${id}`;
    var msg = `Si los datos son correctos presionar "Continuar",si no "Volver" para corregir.`;
    var contenidoHtml = '<pre class="word">' + correo + '</pre><pre>' + identification + '</pre>'
      + '<p>' + msg + '</p>';
    var petition = await this.errorHandlingService.question(contenidoHtml,
      'btn btn-info mt-2', 'btn btn-danger mt-2', 'Continuar', 'volver', 'warning', 'Registro Diócesis de Pasto');
    if (petition === true) {
      this.registerService.register(email, id, "C.C", 1, 1).subscribe(
        resp => {          
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Usuario registrado correctamente')
            this.reset();
          }
          else {
            this.errorHandlingService.negativeAnswer('El usuario ya está registrado');
          }
        }
      )
    }
  }

}
