
import { Component, OnInit, AfterViewInit, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { User } from 'src/app/user/user';
import { InjectTokenService } from 'src/app/sidebar/services/inject-token.service';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { MatTableDataSource } from '@angular/material/table'

import * as htmlToImage from 'html-to-image';
import { MatDialog } from '@angular/material/dialog';
import { FastRegisterComponent } from '../fast-register/fast-register.component';
import { UserService } from 'src/app/shared/components-user/components/user-information/user.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ListUserComponent implements OnInit, AfterViewInit {




  public borrar: boolean = false;
  public edit: boolean = true;
  public user: User;
  public crudUser: CrudPermiss;
  public crudCarnet: CrudPermiss;
  public dataUser = new MatTableDataSource<User>();
  @Output() usuarioSeleccionado: EventEmitter<User>;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  public myForm: FormGroup = new FormGroup({});
  displayedColumns = [
    "names",
    "surnames",
    "documentId",
    "state",
    "nameRole",
    "nameInstitution",
    "carnet",
    'qr',
    "acciones"
  ];


  constructor(private userService: UserService,
    private router: Router,
    private injectTokenService: InjectTokenService,
    private errorHandlingService: ErrorHandlingService, private dialog: MatDialog,) {
    this.usuarioSeleccionado = new EventEmitter();



  }
ok(){
  return true;
}
  ngOnInit() {
    this.dataUser.sort = this.sort;
    this.crudUser = this.injectTokenService.permission('Usuario');
    this.crudCarnet = this.injectTokenService.permission('Administración de carnetización');

    
   
  }

  openDialog() {
    const dialogRef = this.dialog.open(FastRegisterComponent);
    // const snack = this.snackBar.open('Snack bar open before dialog');
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {

      }
    });
  }

  carnet(id: number) {
    this.userService.getCarnetId(id).subscribe(
      resp => {

        if (resp !== false) {
          var url = window.URL.createObjectURL(resp);
          window.open(url);
        } else {
          this.errorHandlingService.negativeAnswer('El usuario no tiene cargada una imagen')
        }

      }
    )
  }

  async deleteUser(id: number, user: string, element) {
    var petition = await this.errorHandlingService.question(`¿Eliminar al usuario ${user}?`,
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {

      this.userService.deleteUser(id).subscribe(
        resp => {
          if (resp.status === 200) {
            this.errorHandlingService.positiveAnswer(`Usuario eliminado`)
            this.dataUser.data = this.dataUser.data.filter(i => i !== element)
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')


          }


        }
      );

    }




  }

  editar(id: string, user: string) {

    this.userService.getUserId(Number(id)).subscribe(
      resp => {
        if (resp.status === 403) {

          this.errorHandlingService.positiveAnswer('El servicio no esta disponible')


        } else {

          this.user = resp;
          this.router.navigate([`/session/user/edit/${id}`]);



        }


      }
    )






  }

  createUserDir() {


    this.router.navigate([`/session/user/create`]);

  }
  view(id: string, user: string) {

    this.userService.getUserId(Number(id)).subscribe(
      resp => {
        if (resp.status === 403) {

          this.errorHandlingService.positiveAnswer('El servicio no esta disponible')


        } else {

          this.user = resp;
          this.router.navigate([`/session/user/view/${id}`]);



        }


      }
    )






  }
  cambiar() {
    this.edit = true;
    this.router.navigate(['session/user/user-edit']);
  }




  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataUser.filter = filterValue.trim().toLowerCase();

    if (this.dataUser.paginator) {
      this.dataUser.paginator.firstPage();
    }
  }


  async ngAfterViewInit() {

    await this.userService.listUser().subscribe(resp => {



      if (resp !== false) {
  

        this.dataUser.data = resp;


      }

    });

    this.dataUser.paginator = this.paginator;



  }


  //errors
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  errorMsgForm(nameFormValue: string, name: string): string {
    const errors = this.myForm.get(nameFormValue)?.errors;
    if (errors) {
      return `Error "${name}" ${this.errorHandlingService.name(errors)}`;
    }
  }
}
