import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';

import { Person } from '../../../profile/interface/data';
import { PersonalService } from 'src/app/shared/components-user/components/personal-information/personal.service';
import { UserService } from 'src/app/shared/components-user/components/user-information/user.service';



@Component({
  selector: 'app-view-create-edit',
  templateUrl: './view-create-edit.component.html',
  styleUrls: ['./view-create-edit.component.css']
})
export class ViewCreateEditComponent implements OnInit {
  
  dataPerson: Person = {
    firstName: "",
    secondName: "",
    firstSurname: "",
    secondSurname: "",
    documentType: "",
    documentId: "",
    cityOfBirth: "",
    dateOfBirth: "",
    bloodType: "",
    contactNumberOne: "",
    contactNumberTwo: "",
    email: "",
    photo: '',
  };
  title: string;
  idUser: string;
  view: boolean = false;
  active: number;
  constructor(

    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute,        

  ) { }
  ngOnInit() {
    this.idUser = this.activatedRoute.snapshot.params.id;


    if (this.idUser) {
      this.userService.getUserId(Number(this.idUser)).subscribe(
        resp => {
          if (resp === false) {
            this.router.navigate([`/session/user/dashboard`]);
          } else {
   
            var view = "/session/user/view";
            var edit = "/session/user/edit";
            this.idUser = this.activatedRoute.snapshot.params.id;
            if ((this.router.url).indexOf(view) === 0) {
              this.view = true;
              this.active = 1;
          
              this.title = 'Ver usuario';
              
            } else if ((this.router.url).indexOf(edit) === 0) {
              this.active = 2;
              this.view = false;
              this.title = 'Editar usuario';
            }
          }
        }
      )
    } else {
      var create = "/session/user/create";
      if ((this.router.url).indexOf(create) === 0) {
        this.active = 0;
        this.title = 'Añadir usuario';
      }
    }
  }


}
