export interface CrudPermiss {

    admin: number;
    create: number;
    read: number;
    update: number;
    delete: number;
}
