import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Observable } from 'rxjs';


import { GlobalService } from 'src/app/service/global.service';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient, private globalService: GlobalService) { }




  getPermissions(idRole:number): Observable<any> {

    const url = `${this.baseUrl}/permission/${idRole}`;


    return this.http.get<any>(url,{observe:'response'}).pipe(

      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
    

      }),
      catchError(err => of(false))
    );
  }
  putPermission(elements: object,idRole: number) {
    
    const url = `${this.baseUrl}/permission/${idRole}`;
    var object;
    let claves = Object.keys(elements);
    for (let i = 0; i < claves.length; i++) {
      let clave = claves[i];
      object = elements[clave];
    }
    var idInformationAcademic = object[0]
    delete object[0]
    


    object[4]=this.number(object[4]);
    object[7]=this.number(object[7]);
    object[10]=this.number(object[10]);
    object[13]=this.number(object[13]);
      var objectFinal={
          "id": object[1],
          "nombre": object[2],
          "admin": object[3],
          "crud": [
              {
                  "id": object[5],
                  "selected": object[4],
                  "type": object[6]
              },
              {
                  "id":object[8],
                  "selected": object[7],
                  "type": object[9]
              },
              {
                  "id": object[11],
                  "selected":object[10],
                  "type": object[12]
              },
              {
                  "id": object[14],
                  "selected": object[13],
                  "type": object[15]
              }
          ]
         }

      
    return this.http.put<any>(url, objectFinal)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }

        }),
        catchError(err => of(false))
      );
  }

  number(value:boolean):number{
    if(value===true){
      return 1;
    }else{
      return 0;
    }
  }
}
