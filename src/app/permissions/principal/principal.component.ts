import { Component, OnInit, AfterViewInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { User } from 'src/app/user/user';
import { InjectTokenService } from 'src/app/sidebar/services/inject-token.service';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { MatTableDataSource } from '@angular/material/table'
import { ModuleService } from 'src/app/module-diocesano/service/module.service';
import { Module } from 'src/app/module-diocesano/interface/module';
import { PermissionsService } from '../services/permissions.service';
import { RoleService } from '../../role-diocesano/service/role.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';
declare const $: any;
@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit, AfterViewInit {
 public borrar: boolean = false;
 public edit: boolean = true;
 public crudPermissions: CrudPermiss;
 public crudRole: CrudPermiss;
 public dataModule = new MatTableDataSource<Module>();
 public myForm: FormGroup = new FormGroup({});
  @ViewChild('matPaginatorFechas', { static: false }) set matPaginatorFechas(value: MatPaginator) {
    this.dataSource.paginator = value;
  }
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @Output() usuarioSeleccionado: EventEmitter<User>;
 public roles;
 public rolesHerencia;
 public showTicks = false;
 public disableSelect = new FormControl(false);
  //--------------------
  public selectedValueHerencia:any
  public selectedValue;
  public dataSource = new MatTableDataSource<any>();
  public inforAcademic: any[];
  public inherit:boolean;
  public errorMsg:boolean;
  public displayedColumns: string[] = [
     'nameModule',
     'admin',
     'selectC',
     'selectR',
    'selectU',
    'selectD',
  ];
  constructor(

    private errorHandlingService: ErrorHandlingService,
    private injectTokenService: InjectTokenService, private permissionService: PermissionsService,
    private formBuilder: FormBuilder
    , private fb: FormBuilder,private roleService:RoleService) {
      this.usuarioSeleccionado = new EventEmitter();
    }
    ngOnInit() {
      this.crudPermissions =   this.injectTokenService.permission('Administración permisos');
      this.crudRole =   this.injectTokenService.permission('Administración de roles');
      this.roleService.listRole().subscribe(
        resp=>{
            this.roles=resp;
            this.rolesHerencia=resp
        }
      )
      this.dataModule.sort = this.sort;
      this.dataSource.sort = this.sort;
      this.myForm = this.fb.group({
        institutionName: this.formBuilder.array([]),
      });
    }
    async login(){
      if(this.inherit===true){
      this.inherit=false;
      (<FormArray>this.myForm.get('institutionName')).controls.forEach((group: FormGroup) => {
        (<any>Object).values(group.controls).forEach((control: FormControl) => {
          const valueNoob=control.value;
          control.setValue(32);
          control.setValue(valueNoob);
          control.markAsTouched();
          control.markAsDirty();
        })
      });
    }
    var petition = await this.errorHandlingService.question('¿Desea guardar los cambios realizados?',
    'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'guardar', 'volver', 'question');
    if (petition === true) {
      var objects = this.getUpdatedValue(this.myForm);
      if ((Object.keys(objects).length === 0) === false) {
        const parseOne = Object.entries(objects).map((e) => ({ [e[0]]: e[1] }));
        var parseTwo = parseOne[0]
        var parseThree = Object.entries(parseTwo).map((e) => ({ [e[0]]: e[1] }));
        var parseFour = parseThree[0];
        var arrayObject = Object.values(Object.entries(parseFour.institutionName).map((e) => ({ [e[0]]: e[1] })));
        var  i=0;
        var  cont=0;
           for (  i = 0; i < arrayObject.length; i++) {
             
               this.permissionService.putPermission(arrayObject[i], this.selectedValue.idRole)
            .subscribe(
              resp => {
                cont++;
                if (resp === true) {
                   this.myForm.reset(this.myForm.value);
                   this.errorMsg=false;
                } else {
                  this.errorMsg=true;
                }
                if((cont)===arrayObject.length){
                    if (this.errorMsg === false) {
                      this.errorHandlingService.positiveAnswer('Permisos actualizados');
                    } else {
                      this.errorHandlingService.negativeAnswer('El servicio no esta disponible');
                    }
                  }
              }
            );
           }
      }
    }
  }
  getUpdatedValue(form: any) {
    var changedValues = {};
    Object.keys(form.controls)
      .forEach(key => {
        let currentControl = form.controls[key];
        if (currentControl.dirty) {
          if (currentControl.controls) {
            changedValues[key] = this.getUpdatedValue(currentControl);
          } else {
            changedValues[1] = form.value.id;
            changedValues[2] = form.value.nombre;
            changedValues[3] = form.value.admin;
            changedValues[4] = form.value.selectC;
            changedValues[5] = form.value.idModuleC;
            changedValues[6] = form.value.typeC;
            changedValues[7] = form.value.selectR;
            changedValues[8] = form.value.idModuleR;
            changedValues[9] = form.value.typeR;
            changedValues[10] = form.value.selectU;
            changedValues[11] = form.value.idModuleU;
            changedValues[12] = form.value.typeU;
            changedValues[13] = form.value.selectD;
            changedValues[14] = form.value.idModuleD;
            changedValues[15] = form.value.typeD;
          }
        }
      });
    return changedValues;
  }
  valueExceptionIdModule(id:number){
    if(id===-1){
      return true;
    }else{
      return false;
    }
  }
  valueExceptionIdModuleAdmin(idC:boolean,idR:boolean,idU:boolean,idD:boolean){
    if(idC===false && idR===false &&idU===false &&idD===false){
      return false;
    }else{
      return true;
    }
  }
   async selectHerencia(){
    var petition = await this.errorHandlingService.question( '¡IMPORTANTE!, recuerde que solo debe heredar un rol cuando este tiene sus permisos vacios, de lo contrario ignore esta opción puede afectar el funcionamiento del sistema.',
    'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Heredar', 'volver', 'warning',
    `Heredar los permisos del rol ${this.selectedValueHerencia.nameRole} a ${this.selectedValue.nameRole}`);
    if (petition === true) {
      this.inherit=true;
      this.getPermiss(this.selectedValueHerencia.idRole);
      this.myForm.markAsDirty();
    }
  }
  getPermiss(idRole:number) {
    this.institutionName.clear();
    this.permissionService.getPermissions(idRole).subscribe(
      resp => {
        if (resp.length !== 0) {
          this.inforAcademic = resp;
          const filtered = this.inforAcademic.filter((element) => {
            element.crud[0].selected=this.boolean(element.crud[0].selected);
            element.crud[1].selected=this.boolean(element.crud[1].selected);
            element.crud[2].selected=this.boolean(element.crud[2].selected);
            element.crud[3].selected=this.boolean(element.crud[3].selected);
            const institutionNameFormGroup = this.formBuilder.group(
              {
                id: [element.id],
                nombre: [element.nombre, [Validators.required]],
                admin: [element.admin, [Validators.required]],
                selectC: [element.crud[0].selected, [Validators.required]],
                idModuleC: [element.crud[0].id, [Validators.required]],
                typeC: [element.crud[0].type, [Validators.required]],
                selectR: [element.crud[1].selected, [Validators.required]],
                idModuleR: [element.crud[1].id, [Validators.required]],
                typeR: [element.crud[1].type, [Validators.required]],
                selectU: [element.crud[2].selected, [Validators.required]],
                idModuleU: [element.crud[2].id, [Validators.required]],
                typeU: [element.crud[2].type, [Validators.required]],
                selectD: [element.crud[3].selected, [Validators.required]],
                idModuleD: [element.crud[3].id, [Validators.required]],
                typeD: [element.crud[3].type, [Validators.required]],
              });
            this.institutionName.push(institutionNameFormGroup);
          });
        }
        const filterPredicate = this.dataSource.filterPredicate;
        this.dataSource.filterPredicate = (data: AbstractControl, filter) => {
          return filterPredicate.call(this.dataSource, data.value, filter);
        }
        this.dataSource.data = this.institutionName.controls;
      }
    )
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.sortData = (data: FormGroup[], sort: MatSort) => {
      const factor =
        sort.direction == "asc" ? 1 : sort.direction == "desc" ? -1 : 0;
      if (factor) {
        data = data.sort((a: FormGroup, b: FormGroup) => {
          const aValue = a.get(sort.active) ? a.get(sort.active).value : null;
          const bValue = a.get(sort.active) ? b.get(sort.active).value : null;
          return aValue > bValue ? factor : aValue < bValue ? -factor : -1;
        });
      }
      return data;
    };
  }
  get institutionName() {
    return this.myForm.get('institutionName') as FormArray;
  }
  boolean(val:number):boolean{
    if(val===0)
    {
      return false;
    }else{
      return true;
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataModule.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
   ngAfterViewInit() {
    if(this.selectedValue !==undefined){
      this.getPermiss(this.selectedValue.idRole);
    }
  }
}
