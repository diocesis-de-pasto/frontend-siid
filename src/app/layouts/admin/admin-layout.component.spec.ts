
import { TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';

import { RouterTestingModule } from '@angular/router/testing';
import { RouterLink, RouterLinkWithHref, RouterOutlet } from '@angular/router';
import { AdminLayoutComponent } from './admin-layout.component';
import { NavbarComponent } from 'src/app/shared/navbar/navbar.component';

describe('AdminLayoutComponent',()=>{

    beforeEach(async () => {
        await TestBed.configureTestingModule({
          imports: [
            RouterTestingModule.withRoutes([])
          ],
          declarations: [
            AdminLayoutComponent
          ],
          schemas:[NO_ERRORS_SCHEMA]
        }).compileComponents();
      });
    it(`Debe de tener un router outlet `, () => {
        const fixture = TestBed.createComponent(AdminLayoutComponent);
        const debugElement= fixture.debugElement.query(By.directive(RouterOutlet));
        expect(debugElement).not.toBeNull();
      });

    
})