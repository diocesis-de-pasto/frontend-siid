
import { FormBuilder, ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ErrorHandlingService } from '../../../service/error-handling.service';
import { NameService } from '../../../service/name.service';
import { RoleService } from '../../service/role.service';
import { CreateRoleComponent } from './create-role.component';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { routes } from '../../../user/user.routing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../../../app.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

describe('CreateRoleComponent', () => {
    let component: CreateRoleComponent;
    let fixture: ComponentFixture<CreateRoleComponent>;
    beforeEach((() => {
        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule, FormsModule, HttpClientModule, MaterialModule,
            BrowserAnimationsModule, RouterTestingModule],
            declarations: [CreateRoleComponent],
            providers: [FormBuilder]

        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(CreateRoleComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('Debe de crear un formulario con dos campos: "nameRole" y "description"', () => {


        expect(component.myForm.contains('nameRole')).toBeTruthy();
        expect(component.myForm.contains('description')).toBeTruthy();
    });

    it('El nombre del rol debe  ser obligatorio', () => {
        const control = component.myForm.get('nameRole');
        control?.setValue('');
        expect(control?.valid).toBeFalsy();
    })

    it('La descripción del rol debe ser obligatorio', () => {
        const control = component.myForm.get('description');
        control?.setValue('prueba descripción ');
        expect(control?.valid).toBeTruthy();
    })





})