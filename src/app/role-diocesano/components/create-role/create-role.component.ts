import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';




import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import {Router} from "@angular/router"
import { RoleService } from '../../service/role.service';
@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['../../../css/formulario.css'],
  encapsulation: ViewEncapsulation.None,
})
export class CreateRoleComponent implements OnInit {
 
  public myForm: FormGroup = new FormGroup({});
  constructor(private fb: FormBuilder,
    private errorHandlingService: ErrorHandlingService,
    private roleService: RoleService,
    private router: Router
  ) {
  }
  ngOnInit() {
    this.myForm = this.fb.group({
      nameRole: [, [Validators.required]],
      description: [, Validators.required],
    });
  }
  verificatePersonal() {
    return true;
  }
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  reset() {
    this.myForm.reset();
    this.myForm.value.refModule = 0;
  }
  async createRole() {
    if (this.myForm.invalid === true) {
      this.myForm.markAllAsTouched();
      this.errorHandlingService.negativeAnswer('Ingrese los campos requeridos')
    } else {
      this.roleService.postRole(
        this.myForm.value.nameRole,
        this.myForm.value.description,
      ).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer('Rol añadido')
            this.router.navigate(['/session/role/principal'])
          } else {
            this.errorHandlingService.negativeAnswer('El rol ya esta registrado')
          
          }
        }
      );
    }
  }
}