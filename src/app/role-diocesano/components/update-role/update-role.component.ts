import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ModuleService } from 'src/app/module-diocesano/service/module.service';
import { Person } from 'src/app/profile/interface/data';

import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { GlobalService } from 'src/app/service/global.service';
import { getUpdatedValue, verifateUnit } from 'src/app/shared/components-user/components/functions';
import { RoleService } from '../../service/role.service';
@Component({
  selector: 'app-update-role',
  templateUrl: './update-role.component.html',
  styleUrls: ['../../../css/formulario.css'],
  encapsulation: ViewEncapsulation.None,
})
export class UpdateRoleComponent implements OnInit {
  public id: string;
  @Input() view: boolean =false;
  public myForm: FormGroup = new FormGroup({});
  constructor(private fb: FormBuilder,
    private errorHandlingService: ErrorHandlingService,
    private roleService:RoleService,
    private activatedRoute:ActivatedRoute,
    ) {
  }
  ngOnInit() {

    if(this.activatedRoute.snapshot.params.idRole){
      this.id = this.activatedRoute.snapshot.params.idRole;
    };
    this.getRol();
    this.myForm = this.fb.group({
      idRole:[],
      nameRole:[, [Validators.required]],
      description: [, Validators.required],
    });
  }
  getRol() {
    this.roleService.getRole(this.id).subscribe(
      resp => {

        if (resp!==false) {
     
          
          this.myForm.controls['idRole'].setValue(resp.idRole);
          this.myForm.controls['nameRole'].setValue(resp.nameRole);
          this.myForm.controls['description'].setValue(resp.description);
        } 
      }
    )
  }
  campoNoValido(campo: string) {
    return this.myForm.get(campo)?.invalid
      && this.myForm.get(campo)?.touched
  }
  async updateRole() {
    var petition = await this.errorHandlingService.question('¿Desea guardar los cambios realizados?',
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'guardar', 'volver', 'question');
    if (petition === true) {
      var change:any = getUpdatedValue(this.myForm);
      this.roleService.putRole(change,     this.myForm.value.idRole)
        .subscribe(
          resp => {
            if (resp === true) {
              this.errorHandlingService.positiveAnswer('Rol actualizado')
              this.getRol();
              this.myForm.reset(this.myForm.value);
            } else {
              this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
            }
          }
        );
    }
  }
}
