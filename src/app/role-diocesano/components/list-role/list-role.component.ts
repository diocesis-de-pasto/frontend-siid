import { Component, OnInit, AfterViewInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { User } from 'src/app/user/user';
import { InjectTokenService } from 'src/app/sidebar/services/inject-token.service';
import { ErrorHandlingService } from 'src/app/service/error-handling.service';
import { MatTableDataSource } from '@angular/material/table'
import { RoleService } from '../../service/role.service';
import { CrudPermiss } from 'src/app/interface/crud-permiss';
@Component({
  selector: 'app-list-role',
  templateUrl: './list-role.component.html',
  styleUrls: ['./list-role.component.css']
})
export class ListRoleComponent implements OnInit, AfterViewInit {
  public borrar: boolean = false;

  public crudRole: CrudPermiss;
  public dataRole = new MatTableDataSource<any>();
  public myForm: FormGroup = new FormGroup({});
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @Output() usuarioSeleccionado: EventEmitter<User>;
  displayedColumns = [
    'nameRole',
    'description',
    'acciones'
  ];
  constructor(
    private errorHandlingService: ErrorHandlingService,
    private router: Router,
    private injectTokenService: InjectTokenService, private _httpClient: HttpClient,
    private roleService: RoleService) {
    this.usuarioSeleccionado = new EventEmitter();
  }
  ngOnInit() {
    this.crudRole = this.injectTokenService.permission('Administración de roles');
    this.dataRole.sort = this.sort;

  }
  createRole() {
    this.router.navigate([`/session/role/create`]);

  }

  async createModule() {
    var petition = await this.errorHandlingService.question(`¿Añadir módulo?`,
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Si', 'No', 'warning');
    if (petition === true) {
      this.router.navigate([`/session/module/create`]);
    }
  }
  async deleteRole(id: number, modulo: string, element) {
    var petition = await this.errorHandlingService.question(`¿Eliminar rol ${modulo}?`,
      'btn btn-success remove mt-2', 'btn btn-danger mt-2', 'Eliminar', 'volver', 'question');
    if (petition === true) {
      this.roleService.deleteRole(id).subscribe(
        resp => {
          if (resp === true) {
            this.errorHandlingService.positiveAnswer(`Rol ${modulo} borrado exitosamente`)
            this.dataRole.data = this.dataRole.data.filter(i => i !== element)
          } else {
            this.errorHandlingService.negativeAnswer('El servicio no esta disponible')
          }
        }
      );
    }
  }
  viewRole(idRole: string) {

    this.router.navigate([`/session/role/view/${idRole}`]);

  }
  editRole(idRole: string) {

    this.router.navigate([`/session/role/edit/${idRole}`]);

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataRole.filter = filterValue.trim().toLowerCase();
    if (this.dataRole.paginator) {
      this.dataRole.paginator.firstPage();
    }
  }
  async ngAfterViewInit() {
    await this.roleService.listRole().subscribe(resp => {
      if (resp !== false) {
        this.dataRole.data = resp;
      }
    });
    this.dataRole.paginator = this.paginator;
  }
}
