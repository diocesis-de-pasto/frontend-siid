import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InjectTokenService } from '../../../sidebar/services/inject-token.service';

import { RoleService } from '../../service/role.service';
@Component({
  selector: 'app-view-create-edit',
  templateUrl: './view-create-edit.component.html',
  styleUrls: ['./view-create-edit.component.css']
})
export class ViewCreateEditComponent implements OnInit {
  public title: string;
  public idRole: string;
  public view: boolean = false;
  public active: number;

  constructor(
    private roleService: RoleService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }
  ngOnInit() {

    this.idRole = this.activatedRoute.snapshot.params.idRole;
  
    if (this.idRole) {
      this.roleService.getRole(this.idRole).subscribe(
        resp => {
          if (resp === false) {
            this.router.navigate([`/session/dashboard`]);
          } else {
            var view = "/session/role/view";
            var edit = "/session/role/edit";
            this.idRole = this.activatedRoute.snapshot.params.id;
            if ((this.router.url).indexOf(view) === 0) {
              this.view = true;
              this.active = 1;
              this.title = 'Ver rol';
            } else if ((this.router.url).indexOf(edit) === 0) {
              this.active = 2;
              this.view = false;
              this.title = 'Editar rol';
            }
          }
        }
      )
    } else {
      var create = "/session/role/create";
      if ((this.router.url).indexOf(create) === 0) {
        this.active = 0;
        this.title = 'Añadir rol';
      }
    }
  }
}
