import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListRoleComponent } from './components/list-role/list-role.component';
import { ViewCreateEditComponent } from './pages/view-create-edit/view-create-edit.component';
export const routes: Routes = [
  {

      path: 'principal',
      component: ListRoleComponent
    },
    {
      path: 'create',
      component: ViewCreateEditComponent
    },
    {
      path: 'edit/:idRole',
      component: ViewCreateEditComponent
    },
    {
      path: 'view/:idRole',
      component: ViewCreateEditComponent
    },
    { path: '**', redirectTo: '/principal'}
  
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleDiocesanoRoutingModule { }
