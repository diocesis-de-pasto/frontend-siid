import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleDiocesanoRoutingModule } from './role-diocesano-routing.module';
import { ListRoleComponent } from './components/list-role/list-role.component';
import { CreateRoleComponent } from './components/create-role/create-role.component';
import { UpdateRoleComponent } from './components/update-role/update-role.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewCreateEditComponent } from './pages/view-create-edit/view-create-edit.component';
@NgModule({
  declarations: [
    ListRoleComponent,
    CreateRoleComponent,
    UpdateRoleComponent,
    ViewCreateEditComponent
  ],
  imports: [
    CommonModule,
    RoleDiocesanoRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class RoleDiocesanoModule { }
