import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private baseUrl: string = environment.baseUrl;
  constructor(private http:HttpClient) { }
  listRole(){
    const url = `${this.baseUrl}/roles`;   
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
        
      }),
      catchError(err=> of(false))                              
    );
  }
  deleteRole(idRole:number){
    const url = `${this.baseUrl}/roles/${idRole}`;   
    return this.http.delete<any>(url).pipe(
      map(resp=> {
        if(resp.status===200){
          return true;
        }else{
          return false;
        }
      }),
      catchError(err => of(false))
    )
  }
  postRole(
    nameRole:  string,
    description: string,
  ): Observable<any> {
    const url = `${this.baseUrl}/roles`;
    const body = {
      nameRole,
      description,
    };
    return this.http.post<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        })
        ,
        catchError(err => of(false))
      );
  }
  getRole(idRole:string) {
    const url = `${this.baseUrl}/roles/${idRole}`;
    return this.http.get<any>(url,{observe:'response'}).pipe(
      map(resp => {
        if(resp.status===204){       
          return false;
        }else{
          return resp.body
        }
        
      }),
      catchError(err => of(false))
    );
  }
  putRole(body: object,idRole: number) {
    const url = `${this.baseUrl}/roles/${idRole}`;
    return this.http.put<any>(url, body)
      .pipe(
        map(resp => {
          if (resp.status === 200) {
            return true;
          } else {
            return false;
          }
        }),
      catchError(err => of(false))
      );
  }
}
